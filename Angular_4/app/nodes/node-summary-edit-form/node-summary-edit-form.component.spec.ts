import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NodeSummaryEditFormComponent } from './node-summary-edit-form.component';

describe('UserEditFormComponent', () => {
  let component: NodeSummaryEditFormComponent;
  let fixture: ComponentFixture<NodeSummaryEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeSummaryEditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeSummaryEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
