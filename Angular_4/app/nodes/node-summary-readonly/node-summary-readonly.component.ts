import { Component, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { NodesService} from '../../core/services';
import { NodesStoreService } from '../../core/store/nodes-store.service';
import { DialogService } from '../../shared/dialog/dialog.service';
import { UsersStoreService } from '../../core/store/users-store.service';
import { NodeSummaryEditComponent } from '../node-summary-edit/node-summary-edit.component';
import { NodeDefinitionUI } from '../../prodapp-core/model';

/**
 * Node summary readonly view
 *
 * @export
 * @class NodeSummaryReadonlyComponent
 * @extends {NodeSummaryEditComponent}
 */
@Component({
  selector: 'sw-node-summary-readonly',
  templateUrl: './node-summary-readonly.component.html',
  styleUrls: ['./node-summary-readonly.component.scss']
})
export class NodeSummaryReadonlyComponent extends NodeSummaryEditComponent {

  /**
   * Node definition for UI
   *
   * @type {NodeDefinitionUI}
   * @memberof NodeSummaryReadonlyComponent
   */
  @Input()
  data: NodeDefinitionUI;

  /**
   * Creates an instance of NodeSummaryReadonlyComponent.
   * @param {NodesService} service - nodes service
   * @param {NodesStoreService} nodesStoreService - nodes store service
   * @param {UsersStoreService} usersStoreService - users store service
   * @param {DialogService} dialogService - dialog service
   * @param {FormBuilder} formBuilder - form builder
   * @memberof NodeSummaryReadonlyComponent
   */
  constructor(
    service: NodesService,
    nodesStoreService: NodesStoreService,
    usersStoreService: UsersStoreService,
    dialogService: DialogService,
    formBuilder: FormBuilder
  ) {
    super(service, nodesStoreService, usersStoreService, dialogService, formBuilder);
  }

}
