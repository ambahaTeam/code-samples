import {
  AlertHandlerTypeId
} from '../model/enum';

import {
  AlertTypeDefinition,
  SmtpAlertTypeDefinition
} from '../model';

/**
 * Checks whether the specified value is instance of {AlertTypeDefinition}.
 *
 * @export
 * @param {*} value - a value to test.
 * @returns {value is AlertTypeDefinition}
 */
export function instanceOfAlertDefinition(value: any): value is AlertTypeDefinition {
    return  value &&
      value.hasOwnProperty('ChangeEntities') &&
      Array.isArray(value['ChangeEntities']);
}

/**
 * Checks whether the specified value is instance of {SmtpAlertTypeDefinition}.
 *
 * @export
 * @param {*} value - a value to test.
 * @returns {value is SmtpAlertTypeDefinition}
 */
export function instanceOfSmtpAlertDefinition(value: any): value is SmtpAlertTypeDefinition {
    if (instanceOfAlertDefinition(value)) {
      return value.AlertHandlers &&
        value.AlertHandlers.length > 0 &&
        value.AlertHandlers[0].AlertHandlerTypeId === AlertHandlerTypeId.Smtp;
    }
}