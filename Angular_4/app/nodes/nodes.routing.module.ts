import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PendingChangesGuard } from '../core/guards';

import { NodeComponent } from './node/node.component';
import { NodePageComponent } from './containers/node-page/node-page.component';
import { NodeAuditPageComponent } from './containers/node-audit-page/node-audit-page.component';
import { NodeHostedServicesComponent } from './containers/node-hosted-services/node-hosted-services.component';

import { EntityNavigationGuard, ContainerNavigationGuard } from '../core/guards';
import { NotFoundComponent } from '../core/not-found/not-found.component';
import { NodeActivityLogsComponent } from './containers/node-activity-logs/node-activity-logs.component';
import { NodeDependenciesPageComponent } from './containers/node-dependencies-page/node-dependencies-page.component';
import { NodesComponent } from './nodes/nodes.component';
import { UserIsNotConsumerGuard } from '../core/guards/user-is-not-consumer.guard';
import { NodesSummaryPageComponent } from './containers/nodes-summary-page/nodes-summary-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: NodesComponent,
        canActivate: [ UserIsNotConsumerGuard, ContainerNavigationGuard ],
        children: [
          {
            path: '',
            canActivate: [ UserIsNotConsumerGuard ],
            component: NodesSummaryPageComponent
          }
        ]
      },
      {
        path: ':id',
        component: NodeComponent,
        canActivate: [EntityNavigationGuard],
        children: [
          {
            path: '',
            component: NodePageComponent,
            canDeactivate: [ PendingChangesGuard ]
          },
          {
            path: 'dependencies',
            component: NodeDependenciesPageComponent,
          },
          {
            path: 'hosted-services',
            component: NodeHostedServicesComponent
          },
          {
            path: 'activity-logs',
            component: NodeActivityLogsComponent
          },
          {
            path: 'audit',
            component: NodeAuditPageComponent
          },
          {
            path: 'monitoring',
            loadChildren: 'app/monitoring/monitoring.module#MonitoringModule'
          }
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ],
  providers: [
    EntityNavigationGuard,
    UserIsNotConsumerGuard,
    ContainerNavigationGuard
  ],
})
export class NodesRoutingModule { }
