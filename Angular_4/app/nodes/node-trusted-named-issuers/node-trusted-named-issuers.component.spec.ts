import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeTrustedNamedIssuersComponent } from './node-trusted-named-issuers.component';

describe('NodeTrustedNamedIssuersComponent', () => {
  let component: NodeTrustedNamedIssuersComponent;
  let fixture: ComponentFixture<NodeTrustedNamedIssuersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeTrustedNamedIssuersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeTrustedNamedIssuersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
