import { Component, OnDestroy } from '@angular/core';
import { NodeDefinition } from '../../../prodapp-core/model';
import {
  NodesStoreService,
  UsersStoreService
} from '../../../core/store';

/**
 * A component that handles the {NodeDefinition} edit page logic.
 */
@Component({
  selector: 'sw-node-page',
  templateUrl: './node-page.component.html',
  styleUrls: ['./node-page.component.scss']
})
export class NodePageComponent {

  /**
   * Gets observable of {NodeDefinition} this component is bound to.
   * @memberof NodePageComponent
   */
  public readonly node$ = this._nodeStoreService.getSelected();

  /**
   * Gets the observable that can be used to track pending changes state.
   * @memberof NodePageComponent
   */
  public readonly hasPendingChanges$ = this._nodeStoreService.hasPendingChanges();

  /**
   * Gets observable with current user data
   *
   * @memberof NodePageComponent
   */
  public readonly user$ = this._usersStoreService.getSelected();

  /**
   * Check if user has write access to page
   *
   *
   * @memberof NodePageComponent
   */
  public readonly canWrite$ = this._nodeStoreService.canWrite();

  /**
   * Creates an instance of NodePageComponent.
   * @param {NodeStoreService} _nodeStoreService - the store service that provides access to the node state.
   * @param {UsersStoreService} _usersStoreService - the store service that provides access to the user state
   * @memberof NodePageComponent
   */
  constructor(private readonly _nodeStoreService: NodesStoreService,
              private readonly _usersStoreService: UsersStoreService) {
  }

  /**
   * Saves the updated node definition into the store.
   * @param {NodeDefinition} node - the updated {NodeDefinition} data.
   */
  onSave(node: NodeDefinition) {
    this._nodeStoreService.save(node);
  }

  /**
   * Refreshes the current node definition from the store.
   * @param id - the node id.
   */
  onRefresh(id: number) {
    this._nodeStoreService.refresh(id);
  }

  /**
   * Deletes an node.
   *
   * @param {NodeDefinition} node - an node to delete.
   */
  onDelete(node: NodeDefinition) {
    if (node) {
      this._nodeStoreService.delete();
    }
  }

}
