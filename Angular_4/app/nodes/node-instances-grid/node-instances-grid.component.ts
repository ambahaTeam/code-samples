import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DataTableRowExpand } from '../../shared/data-table/data-table-row-events';

import {
  ChangeSet,
  NodeDefinition,
  NodeInstance,
} from '../../prodapp-core/model';
import { DialogService } from '../../shared/dialog/dialog.service';
import { ImageFactory } from '../../prodapp-core/utils/ImageFactory';
import { DELETE_CONFIRMATION, DELETE } from '../../prodapp-core/constants';

/**
 * Component with node instances
 *
 * @export
 * @class NodeInstancesGridComponent
 */
@Component({
  selector: 'sw-node-instances',
  templateUrl: './node-instances-grid.component.html',
  styleUrls: ['./node-instances-grid.component.scss']
})
export class NodeInstancesGridComponent {

  /**
   * Gets or sets the node definition this component is bound to.
   *
   * @type {ChangeSet[]}
   * @memberof NodeInstancesGridComponent
   */
  @Input()
  data: NodeDefinition;

  /**
   * Gets or sets readonly setting for component
   *
   *
   * @memberof NodeSummaryEditFormComponent
   */
  @Input()
  readonly = false;

  /**
   * Fired when the user expand the instance.
   *
   * @type {EventEmitter<DataTableRowExpand<ChangeSet>>}
   * @memberof NodeInstancesGridComponent
   */
  @Output()
  readonly expand = new EventEmitter<DataTableRowExpand<ChangeSet>>();

  /**
   * Node instance delete event emitter
   *
   *
   * @memberof NodeInstancesGridComponent
   */
  @Output()
  readonly delete = new EventEmitter<NodeInstance>();

  /**
   * Creates an instance of NodeInstancesGridComponent.
   * @param {DialogService} _dialogService - dialog service
   *
   * @memberof NodeInstancesGridComponent
   */
  constructor(private readonly _dialogService: DialogService) {}

  /**
   * Handles detail row expand event. Emits expand event.
   *
   * @param {DataTableRowExpand<ChangeSet>} expand - the row expand event arguments.
   *
   * @memberof NodeInstancesGridComponent
   */
  public onExpand(expand: DataTableRowExpand<ChangeSet>) {
    this.expand.emit(expand);
  }

  /**
   * Handles delete event
   *
   * @param {any} instance - wrap object with node instance inside
   *
   * @memberof NodeInstancesGridComponent
   */
  public onDelete(instance: {data: NodeInstance, id: number}) {
    const header = `${DELETE} Node Instance`;
    const message = `${DELETE_CONFIRMATION} selected Node Instance '${instance.data.Name}'?`;
    this._dialogService.confirm(header, message).afterClosed()
      .subscribe(confirm => {
        if (confirm) {
          this.delete.emit(instance.data);
      }
    });
  }

  /**
   * Get status image path for provided node status value
   *
   * @param {number} status - node status value
   * @returns Path to status image
   *
   * @memberof NodeInstancesGridComponent
   */
  public getStatusImage(status: number) {
    return ImageFactory.getNodeStatusImage(status);
  }
}
