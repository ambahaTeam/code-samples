import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeTrustedIssuerEditWizardComponent } from './node-trusted-issuer-edit-wizard.component';

describe('NodeTrustedIssuerEditWizardComponent', () => {
  let component: NodeTrustedIssuerEditWizardComponent;
  let fixture: ComponentFixture<NodeTrustedIssuerEditWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeTrustedIssuerEditWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeTrustedIssuerEditWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
