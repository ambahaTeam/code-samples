﻿import '@ngrx/core/add/operator/select';
import { compose } from '@ngrx/core/compose';
import { combineReducers } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { storeLogger } from 'ngrx-store-logger';
import { createSelector } from 'reselect';

import {
    DomainObjectBase,
    EntityType,
} from '../prodapp-core/model';

import { environment } from '../../environments/environment';
import * as fromSystem from './system-info.reducer';
import * as fromProcessing from './processing.reducer';
import * as fromRepository from './repository.reducer';
import * as fromUsers from './user.reducer';
import * as fromErrors from './error.reducer';
import * as fromSummary from './summary.reducer';
import * as fromBinding from './binding.reducer';
import * as fromBehavior from './behavior.reducer';
import * as fromCustomEntities from './custom-entity.reducer';
import * as fromFiles from './files.reducer';
import * as fromApp from './app.reducer';
import * as fromDomain from './domain.reducer';
import * as fromSelected from './selected.reducer';
import * as fromAlertTypes from './alert-type.reducer';
import * as fromAudit from './audit.reducer';
import * as fromUserSessions from './user-session.reducer';
import * as fromNodes from './node.reducer';
import * as fromServices from './service.reducer';
import * as fromAuthorizationRules from './authorization-rules.reducer';
import * as fromServiceAgreements from './service-agreement.reducer';
import * as fromServiceVersions from './service-version.reducer';
import * as fromDependencies from './dependency.reducer';
import { IDependencyState } from '../prodapp-core/dependency';
import * as fromFolders from './folder.reducer';
import * as fromServiceImport from './service-import.reducer';
import * as fromFolderEntityTypes from './folder-entity-types.reducer';
import * as fromRepositoryExport from './repository-export.reducer';
import * as fromRepositoryImport from './repository-import.reducer';
import * as fromAuthExpressionDesigner from './authorization-expression-designer.reducer';
import * as fromMonitoring from './monitoring.reducer';
import * as fromViolations from './violations.reducer';
import * as fromAlertHandlerTypes from './alert-handler-types.reducer';
import * as fromTransactions from './transactions.reducer';
import * as fromReadings from './readings.reducer';
import * as fromCertificates from './certificates.reducer';

export interface AppState {
    behaviors: fromBehavior.State;
    nodes: fromNodes.State;
    authorizationRules: fromAuthorizationRules.IAuthorizationRulesState;
    serviceAgreements: fromServiceAgreements.State;
    bindings: fromBinding.State;
    error: fromErrors.State;
    processing: fromProcessing.State;
    repository: fromRepository.State;
    summary: fromSummary.State;
    system: fromSystem.State;
    users: fromUsers.State;
    router: fromRouter.RouterState;
    customEntities: fromCustomEntities.State;
    files: fromFiles.State;
    app: fromApp.AppState;
    selected: fromSelected.State;
    alertTypes: fromAlertTypes.IAlertTypeState;
    audit: fromAudit.IAuditState;
    changeRecordDetails: fromAudit.IChangeRecordDetailsState;
    userSessions: fromUserSessions.UserSessionsState;
    services: fromServices.State;
    serviceVersions: fromServiceVersions.State;
    dependencies: IDependencyState;
    folders: fromFolders.State;
    serviceImport: fromServiceImport.ServiceImportState;
    folderEntityTypes: fromFolderEntityTypes.IFolderEntityTypeState;
    repositoryExport: fromRepositoryExport.IRepositoryExportState;
    repositoryImport: fromRepositoryImport.IRepositoryImportState;
    authExpression: fromAuthExpressionDesigner.IAuthorizationExpressionDesignerState;
    monitoring: fromMonitoring.IMonitoringState;
    violations: fromViolations.IViolationsState;
    alertHandlerTypes: fromAlertHandlerTypes.IAlertHandlerTypesState;
    transactions: fromTransactions.ITransactionsState;
    readings: fromReadings.IReadingsState;
    certificates: fromCertificates.ICertificatesState;
}

/**
 * This map is used to map EntityTypes to strings
 * The string represents the name of the slice in the AppState for an EntityType
 * This is to ensure that our state is human readable, which helps with development and debug
 * TODO: Discuss with team -- it may be that we're okay operating with the enum value in the app state
 */
export const AppDomains = {
  [EntityType.Folder]: 'folders',
  [EntityType.Behavior]: 'behaviors',
  [EntityType.Binding]: 'bindings',
  [EntityType.User]: 'users',
  [EntityType.Custom]: 'customEntities',
  [EntityType.AlertType]: 'alertTypes',
  [EntityType.Node]: 'nodes',
  [EntityType.AuthorizationRule]: 'authorizationRules',
  [EntityType.ServiceAgreement]: 'serviceAgreements',
  [EntityType.Service]: 'services',
  [EntityType.ServiceVersion]: 'serviceVersions',
  [EntityType.File]: 'files',
};

const reducers = {
    behaviors: fromDomain.composeDomain(fromBehavior.reducer, EntityType.Behavior),
    nodes: fromDomain.composeDomain(fromNodes.reducer, EntityType.Node),
    bindings: fromDomain.composeDomain(fromBinding.reducer, EntityType.Binding),
    error: fromErrors.reducer,
    processing: fromProcessing.reducer,
    repository: fromRepository.reducer,
    summary: fromSummary.reducer,
    system: fromSystem.reducer,
    users: fromDomain.composeDomain(fromUsers.reducer, EntityType.User),
    router: fromRouter.routerReducer,
    customEntities: fromDomain.composeDomain(fromCustomEntities.reducer, EntityType.Custom),
    files: fromDomain.composeDomain(fromFiles.reducer, EntityType.File),
    app: fromApp.reducer,
    selected: fromSelected.reducer,
    alertTypes: fromAlertTypes.reducer,
    audit: fromAudit.reducer,
    changeRecordDetails: fromAudit.changeRecordDetailsReducer,
    userSessions: fromUserSessions.reducer,
    services: fromDomain.composeDomain(fromServices.reducer, EntityType.Service),
    authorizationRules: fromDomain.composeDomain(fromAuthorizationRules.reducer, EntityType.AuthorizationRule),
    serviceAgreements: fromDomain.composeDomain(fromServiceAgreements.reducer, EntityType.ServiceAgreement),
    serviceVersions: fromDomain.composeDomain(fromServiceVersions.reducer, EntityType.ServiceVersion),
    dependencies: fromDependencies.reducer,
    folders: fromDomain.composeDomain(fromFolders.reducer, EntityType.Folder),
    serviceImport: fromServiceImport.reducer,
    folderEntityTypes: fromFolderEntityTypes.reducer,
    repositoryExport: fromRepositoryExport.reducer,
    repositoryImport: fromRepositoryImport.reducer,
    authExpression: fromAuthExpressionDesigner.reducer,
    monitoring: fromMonitoring.reducer,
    violations: fromViolations.reducer,
    alertHandlerTypes: fromAlertHandlerTypes.reducer,
    transactions: fromTransactions.reducer,
    readings: fromReadings.reducer,
    certificates: fromCertificates.reducer
};

const developmentReducer =  compose(storeLogger({ collapsed: true}), combineReducers)(reducers);
const productionReducer =  combineReducers(reducers);

export function reducer( state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}


/**
 * Application Selectors
 * All of our projections are defined here
 */

// ========================= Root ===============================================
export const getState           = (state: AppState) => state;
// ========================= System =============================================
export const getSystemState              = (state: AppState) => state.system;
export const getSystemInfo               = createSelector(getSystemState, fromSystem.getSystemInfo);
export const getUserFolder               = createSelector(getSystemState, fromSystem.getUserFolder);
export const getUserAccess               = createSelector(getSystemState, fromSystem.getUserAccess);
export const isAdministrator             = createSelector(getSystemState, fromSystem.isAdministrator);
export const isConsumer                  = createSelector(getSystemState, fromSystem.isConsumer);
export const getRoutedFromComponentName  = createSelector(getSystemState, fromSystem.getRoutedFromComponentName);
export const getProcessing               = (state: AppState) => state.processing;
export const getErrorState               = (state: AppState) => state.error;
export const getError                    = createSelector(getErrorState, fromErrors.getError);
export const getErrorModalState          = createSelector(getErrorState, fromErrors.getErrorModalState);
export const getFilesState               = (state: AppState) => state.files;
export const getFileSelectorClearedState = createSelector(getFilesState, fromFiles.getFileSelectorCleared);
export const getAppState                 = createSelector(getState, (state) => state.app);
export const canNavigate                 = createSelector(getAppState, fromApp.canNavigate);
export const getNavigationDisabledReason = createSelector(getAppState, fromApp.navigationDisabledReason);
export const getRepositoryDisplayedState = createSelector(getAppState, fromApp.isRepositoryOpen);
export const getSearchDisplayedState     = createSelector(getAppState, fromApp.isSearchOpen);

// ========================= Domain Entities =====================================
export const getSummaryState            = (state: AppState) => state.summary;

// USER SESSIONS ================================================================================================
export const getUserSessionsState       = (state: AppState) => state.userSessions;
export const getUserSessionsSearchRequest = (id: number) =>
    createSelector(getUserSessionsState, (state) => fromUserSessions.getRequest(state, id));
export const getUserSessions            = (id: number) =>
    createSelector(getUserSessionsState, (state) => fromUserSessions.getSessions(state, id));

// REPOSITORY ================================================================================================
export const getRepositoryState              = (state: AppState) => state.repository;
export const getSelectedTreeNode             = createSelector(getRepositoryState, (state) => state.selected.current);
export const getRepositoryEntities           = createSelector(getRepositoryState, (state) => state.entities);
export const getActiveRepositoryNodeTypes    = createSelector(getRepositoryState, fromRepository.getFilter);
export const getAvailableRepositoryFilters   = createSelector(getRepositoryState, fromRepository.getAvailableFilters);
export const getSearchTerm                   = createSelector(getRepositoryState, fromRepository.getSearchTerm );

export const getRepositoryFolder = (id: number) =>
    createSelector(getRepositoryState, fromRepository.getFolder(id));

export const getSelectedRepositoryBehaviors = (ids: number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedBehaviors(ids));

export const getSelectedRepositoryBindings = (ids: number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedBindings(ids));

export const getSelectedRepositoryCustomEntities = (ids: number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedCustomEntities(ids));

export const getSelectedRepositoryNodes = (ids:  number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedNodes(ids));

export const getSelectedRepositoryRules = (ids:  number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedRules(ids));

export const getSelectedRepositoryServiceAgreements = (ids:  number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedServiceAgreements(ids));

export const getSelectedRepositoryServices = (ids:  number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedServices(ids));

export const getSelectedRepositoryVirtualServices = (ids:  number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedVirtualServices(ids));

export const getSelectedRepositoryUsers = (ids:  number[]) =>
    createSelector(getRepositoryState, fromRepository.getSelectedUsers(ids));


// ========================= Base Selectors =====================================
export function getDomainState<T extends DomainObjectBase>(type: EntityType) {
    return createSelector<AppState, AppState, fromDomain.DomainState<T>>(getState, (state) => state[AppDomains[type]]);
}

export function getDomainObject<T extends DomainObjectBase>(id: number, type: EntityType) {
    return createSelector(getDomainState<T>(type), (state) => state.entities.getValue(id));
}

// ========================= Selected Selectors =================================
export const getSelectedState    = (state: AppState) => state.selected;


