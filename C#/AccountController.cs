﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Braintree;
using LearnLms.BusinessLogic;
using LearnLms.BusinessLogic.Models.Identity;
using LearnLms.Domain.Models.Identity;
using LearnLms.Domain.Services;
using LearnLms.Web.New.Controllers.Common;
using LearnLms.Web.New.Models.AccountViewModels;
using LearnLms.Web.New.Models.CheckoutViewModels;
using LearnLms.Web.New.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using NLog;
using Crypto = CryptoHelper.Crypto;

namespace LearnLms.Web.New.Areas.User.Controllers
{
    [Authorize(Policy = "AuthorisedOnly")]
    [Area("User")]
    public class AccountController : AuthController
    {
        private readonly BraintreeOptions _braintreeOptions;
        private readonly EmailOptions _options;
        private readonly IEmailSender _sender;
        private readonly ITransactionService _transactionService;
        private BraintreeGateway _gateway;

        public AccountController(IAuthenticationService authenticationService, 
                                 ITransactionService transactionService,
                                 IEmailSender sender,
                                 IOptions<EmailOptions> options, ICourseService courseService, 
                                 IOptions<BraintreeOptions> btOptions)
            : base(authenticationService, courseService)
        {
            _sender = sender;
            _options = options.Value;
            _braintreeOptions = btOptions.Value;
            _transactionService = transactionService;
        }

        protected override Logger Logger => LogManager.GetCurrentClassLogger();

        private BraintreeGateway Gateway
        {
            get
            {
                if (_gateway == null)
                {
                    _gateway = new BraintreeGateway(_braintreeOptions.Environment, _braintreeOptions.MerchantId,
                        _braintreeOptions.PublicKey, _braintreeOptions.PrivateKey);
                }
                ;
                return _gateway;
            }
        }

        [AllowAnonymous]
        public IActionResult SignUp()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult SignIn(string returnUrl)
        {
            return View(new
            {
                ReturnUrl = returnUrl
            });
        }

        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult ChangePassword(string guid, string hash)
        {
            return View(new
            {
                Id = guid,
                Hash = hash
            });
        }

        /// <summary>
        ///     Profile page
        /// </summary>
        /// <returns></returns>
        public IActionResult Profile()
        {
            return View(GetCurrentUser());
        }

        /// <summary>
        ///     Orders page
        /// </summary>
        /// <returns></returns>
        public IActionResult Orders()
        {
            try
            {
                return View(_transactionService.GetUserTransactions(CurrentUser.Id));
            }
            catch (Exception ex)
            {
                return HandleError(ex);
            }
        }

        /// <summary>
        ///     Checkout page with payment
        /// </summary>
        /// <returns></returns>
        public IActionResult Checkout(CheckoutViewModel model = null)
        {
            try
            {
                var clientToken = Gateway.ClientToken.generate();
                ViewBag.ClientToken = clientToken;
                if (model == null)
                {
                    model = new CheckoutViewModel();
                }
                model.Courses = GetCoursesForCart();
                return View(model);
            }
            catch (Exception ex)
            {
                return HandleError(ex);
            }
        }

        /// <summary>
        ///     Checkout finished page
        /// </summary>
        /// <returns></returns>
        public IActionResult CheckoutFinished()
        {
            return View();
        }

        /// <summary>
        ///     Sign Up (register)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SignUp(UserRegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new UserModel
                    {
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Password = model.Password,
                        Id = Guid.NewGuid(),
                        UserGroups = new List<IUserGroup>
                        {
                            new UserGroupModel
                            {
                                Group = new GroupModel
                                {
                                    GroupId = (long) Enums.UserGroups.User
                                }
                            }
                        }
                    };

                    if (AuthenticationService.Register(user))
                    {
                        await SignInAsync(user, true);
                        return Json(true);
                    }
                }
                else
                {
                    ParseValidationErrorsAndThrowException();
                }
                return HandleJsonError("Sign Up failed.");
            }
            catch (Exception ex)
            {
                return HandleJsonError(ex);
            }
        }

        /// <summary>
        ///     Sign In
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SignIn(LoginViewModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user =
                        await
                            AuthenticationService.Login(new LoginModel {Email = model.Email, Password = model.Password});
                    if (user != null)
                    {
                        await SignInAsync(user, model.RememberMe);
                        return Json(user);
                    }
                }
                else
                {
                    ParseValidationErrorsAndThrowException();
                }
                return Json(false);
            }
            catch (Exception ex)
            {
                return HandleJsonError(ex);
            }
        }

        [HttpPost]
        public IActionResult ManageProfile(UserManageViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new UserModel
                    {
                        Id = model.Id,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        PasswordHash = Crypto.HashPassword(model.Password)
                    };
                    AuthenticationService.UpdateUser(user);
                    return Json(true);
                }
                ParseValidationErrorsAndThrowException();
                return Json(false);
            }
            catch (Exception ex)
            {
                return HandleJsonError(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = AuthenticationService.FindUserById(model.Id);
                    if ((user != null)
                        && (model.Password == model.ConfirmPassword)
                        && !string.IsNullOrEmpty(user.ForgotPasswordHash)
                        && (user.ForgotPasswordHash == model.Hash))
                    {
                        user.PasswordHash = Crypto.HashPassword(model.Password);
                        user.ForgotPasswordHash = null;
                        AuthenticationService.UpdateUser(user);
                        await SignInAsync(user, false);
                        return Json(true);
                    }
                }
                else
                {
                    ParseValidationErrorsAndThrowException();
                }
                return Json(false);
            }
            catch (Exception ex)
            {
                return HandleJsonError(ex);
            }
        }

        /// <summary>
        ///     Sign Out
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SignOut()
        {
            try
            {
                await AuthenticationManager.SignOutAsync("LmsAuth");
                return Json("OK");
            }
            catch (Exception ex)
            {
                return HandleJsonError(ex);
            }
        }

        /// <summary>
        ///     Get current user data
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetUser()
        {
            var user = GetCurrentUser();
            return Json(user);
        }

        /// <summary>
        ///     Get current user data
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            try
            {
                var user = AuthenticationService.FindUserByEmail(email);
                if (user != null)
                {
                    user.ForgotPasswordHash = Crypto.HashPassword(Guid.NewGuid().ToString());
                    AuthenticationService.UpdateUser(user);
                    var forgotPasswordUrl = Url.Action("ChangePassword", "Account", new
                    {
                        area = "User",
                        guid = user.Id,
                        hash = user.ForgotPasswordHash
                    }, Request.Scheme);
                    await _sender.SendEmailAsync(email, _options.ForgotPasswordSubject,
                        string.Format(_options.ForgotPasswordMessageFormat, forgotPasswordUrl));
                    return Json(true);
                }
                return Json(false);
            }
            catch (Exception ex)
            {
                return HandleJsonError(ex);
            }
        }

        [HttpPost]
        public IActionResult SubmitPayment(PaymentViewModel model)
        {
            try
            {
                // Check payment sum with ordered courses sum
                var courses = GetCoursesForCart().ToList();
                if ((courses == null) || (courses.Sum(c => c.Price) != model.Amount))
                {
                    throw new Exception("Payment amount must equal to summary price of ordered courses.");
                }

                var courseIds = courses.Select(c => c.Id).ToArray();

                //Validate if user tries to buy existing course again
                _transactionService.ValidateCoursesBeforeBuying(courseIds, CurrentUser.Id);

                var request = new TransactionRequest
                {
                    Amount = model.Amount,
                    PaymentMethodNonce = model.payment_method_nonce,
                    Options = new TransactionOptionsRequest
                    {
                        SubmitForSettlement = true
                    }
                };

                var result = Gateway.Transaction.Sale(request);
                if (result.IsSuccess())
                {
                    _transactionService.BuyCourses(result.Target.Id, model.Amount, courseIds, CurrentUser.Id);
                    Response.Cookies.Delete("cart");
                    return RedirectToAction("CheckoutFinished");
                }
                //TODO: what is that?
                if (result.Transaction != null)
                {
                    return RedirectToAction("CheckoutFinished");
                }
                var errorMessages = "";
                foreach (var error in result.Errors.DeepAll())
                    errorMessages += "Error: " + error.Message + "\n";
                Logger.Log(LogLevel.Error, new Exception(errorMessages));
                return RedirectToAction("Checkout", new {errors = errorMessages});
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex);
                return RedirectToAction("Checkout", new {errors = ex.Message});
            }
        }

        private async Task SignInAsync(IUser user, bool isPersistent)
        {
            await AuthenticationManager.SignOutAsync("LmsAuth");
            var claims = new List<Claim>
            {
                new Claim("Authorised", "true"),
                new Claim("UserId", user.Id.ToString())
            };

            if (user.UserGroups.Any(ug => ug.Group.GroupId == (int) Enums.UserGroups.Admin))
            {
                claims.Add(new Claim("IsAdmin", ""));
            }

            var userIdentity = new ClaimsIdentity(claims);
            var userPrincipal = new ClaimsPrincipal(userIdentity);
            await AuthenticationManager.SignInAsync("LmsAuth", userPrincipal, new AuthenticationProperties
            {
                IsPersistent = isPersistent,
                ExpiresUtc = DateTimeOffset.UtcNow.AddDays(7)
            });
        }

        private void ParseValidationErrorsAndThrowException()
        {
            throw new Exception(string.Join("<br/>",
                from m in ModelState.Values
                where m.ValidationState == ModelValidationState.Invalid
                select m.Errors[0].ErrorMessage));
        }
    }
}