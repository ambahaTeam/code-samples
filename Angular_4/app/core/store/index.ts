import { AuditStoreService } from './audit-store.service';
import { UsersStoreService } from './users-store.service';
import { BindingsStoreService } from './bindings-store.service';
import { AlertTypesStoreService } from './alert-types-store.service';
import { CustomEntitiesStoreService } from './custom-entities-store.service';
import { FoldersStoreService } from './folders-store.service';
import { BehaviorsStoreService } from './behaviors-store.service';
import { NodesStoreService } from './nodes-store.service';
import { FilesStoreService } from './files-store.service';
import { SystemInfoStoreService } from './system-info-store.service';
import { ServicesStoreService } from './services-store.service';
import { ServiceVersionsStoreService } from './service-versions-store.service';
import { DependenciesStoreService } from './dependencies-store.service';
import { RepositoryStoreService } from './repository-store.service';
import { RestServiceImportStoreService } from './rest-service-import-store.service';
import { SoapServiceImportStoreService } from './soap-service-import-store.service';
import { SoapServiceVersionsStoreService } from './soap-service-versions-store.service';
import { RestServiceVersionsStoreService } from './rest-service-versions-store.service';
import { AuthorizationRulesStoreService } from './authorization-rules-store.service';
import { ServiceAgreementsStoreService } from './service-agreements-store.service';
import { FolderEntityTypesStoreService } from './folder-entity-types-store.service';
import { RepositoryExportStoreService } from './repository-export-store.service';
import { RepositoryImportStoreService } from './repository-import-store.service';
import { AuthorizationExpressionDesignerStoreService } from './authorization-expression-designer-store.service';
import { DataSchemaElementsStoreService } from './data-schema-elements-store.service';
import { MonitoringStoreService } from './monitoring-store.service';
import { ViolationsStoreService } from './violations-store.service';
import { ReadingsStoreService } from './readings-store.service';
import { AlertHandlerTypesStoreService } from './alert-handler-types-store.service';
import { TransactionsStoreService } from './transactions-store.service';
import { CertificatesStoreService } from './certificates-store.service';

/** Contains all store services for batch injection */
export const StoreServices = [
  AuditStoreService,
  UsersStoreService,
  BindingsStoreService,
  AlertTypesStoreService,
  CustomEntitiesStoreService,
  FoldersStoreService,
  BehaviorsStoreService,
  FilesStoreService,
  BehaviorsStoreService,
  NodesStoreService,
  SystemInfoStoreService,
  ServicesStoreService,
  ServiceVersionsStoreService,
  SoapServiceVersionsStoreService,
  RestServiceVersionsStoreService,
  DependenciesStoreService,
  RepositoryStoreService,
  AuthorizationRulesStoreService,
  RestServiceImportStoreService,
  SoapServiceImportStoreService,
  ServiceAgreementsStoreService,
  RepositoryExportStoreService,
  FolderEntityTypesStoreService,
  RepositoryImportStoreService,
  AuthorizationExpressionDesignerStoreService,
  DataSchemaElementsStoreService,
  MonitoringStoreService,
  ViolationsStoreService,
  AlertHandlerTypesStoreService,
  TransactionsStoreService,
  ReadingsStoreService,
  CertificatesStoreService
];

export * from './audit-store.service';
export * from './users-store.service';
export * from './bindings-store.service';
export * from './alert-types-store.service';
export * from './custom-entities-store.service';
export * from './folders-store.service';
export * from './behaviors-store.service';
export * from './files-store.service';
export * from './nodes-store.service';
export * from './system-info-store.service';
export * from './services-store.service';
export * from './service-versions-store.service';
export * from './dependencies-store.service';
export * from './repository-store.service';
export * from './authorization-rules-store.service';
export * from './service-agreements-store.service';
export * from './rest-service-import-store.service';
export * from './soap-service-import-store.service';
export * from './rest-service-versions-store.service';
export * from './soap-service-versions-store.service';
export * from './repository-export-store.service';
export * from './folder-entity-types-store.service';
export * from './repository-import-store.service';
export * from './authorization-expression-designer-store.service';
export * from './data-schema-elements-store.service';
export * from './monitoring-store.service';
export * from './violations-store.service';
export * from './readings-store.service';
export * from './alert-handler-types-store.service';
export * from './transactions-store.service';
