import { Component, Inject, ViewChild } from '@angular/core';
import { KeyInfo } from '../../prodapp-core/model/KeyInfo';
import { DialogRef, SW_DIALOG_DATA } from '../dialog';
import { KeyEditFormComponent } from './key-edit-form/key-edit-form.component';

/**
 * Interface for object which should be provided for key edit dialog
 *
 * @export
 * @interface KeyEditDialogData
 */
export interface KeyEditDialogData {
  header: string;
  entity: KeyInfo;
  readOnly: boolean;
}

/**
 * Component for editing of key and name data for different objects, like nodes, behaviors, etc.
 *
 * @export
 * @class KeyEditDialogComponent
 */
@Component({
  selector: 'sw-key-edit-dialog',
  templateUrl: './key-edit-dialog.component.html',
  styleUrls: ['./key-edit-dialog.component.scss']
})
export class KeyEditDialogComponent  {

  /**
   * Gets or sets readonly flag for component
   *
   *
   * @memberof KeyEditDialogComponent
   */
  readOnly = false;

  /**
   * Child form for key editing
   *
   * @type {KeyEditFormComponent}@memberof KeyEditDialogComponent
   */
  @ViewChild(KeyEditFormComponent)
  keyEditForm: KeyEditFormComponent;

  /**
   * Creates an instance of KeyEditDialogComponent.
   * @param {DialogRef<KeyEditDialogComponent>} dialogRef
   * @param {KeyEditDialogData} data
   * @param {FormBuilder} formBuilder
   *
   * @memberof KeyEditDialogComponent
   */
  constructor(
    private dialogRef: DialogRef<KeyEditDialogComponent>,
    @Inject(SW_DIALOG_DATA) public data: KeyEditDialogData
  ) {
    this.readOnly = data.readOnly;
  }

  /**
   * Sends notification for all subscribed components that key data was changed
   *
   *
   * @memberof KeyEditDialogComponent
   */
  sendSavedNotification() {
    this.dialogRef.close(this.data.entity);
  }
}
