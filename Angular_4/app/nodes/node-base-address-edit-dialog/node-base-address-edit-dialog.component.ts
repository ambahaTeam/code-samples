import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { NodeAddress } from '../../prodapp-core/model';

import { SW_DIALOG_DATA } from '../../shared/dialog/dialog-injector';
import { DialogRef } from '../../shared/dialog/dialog-ref';
import { FormValidators } from '../../shared/validation/form-validators';
import { ValidationMessages } from '../../shared/validation/messages';
import { NodesStoreService } from '../../core/store';
import { Uri } from '../../prodapp-core/utils';

import { EditComponentBase } from '../../shared/components/base-edit.component';

/**
 * Dialog component for node base address edit
 *
 * @export
 * @class NodeBaseAddressEditDialogComponent
 * @extends {EditComponentBase<NodeAddress>}
 */
@Component({
  selector: 'sw-node-base-address-edit-dialog',
  templateUrl: './node-base-address-edit-dialog.component.html',
  styleUrls: ['./node-base-address-edit-dialog.component.scss']
})
export class NodeBaseAddressEditDialogComponent extends EditComponentBase<NodeAddress> {

  private _address: Uri;

  /**
   * Commit event emitter
   *
   * @type {EventEmitter<NodeAddress>}
   * @memberof NodeBaseAddressEditDialogComponent
   */
  @Output()
  commit: EventEmitter<NodeAddress> = new EventEmitter<NodeAddress>();

  /**
   * Current form group
   *
   * @type {FormGroup}
   * @memberof NodeBaseAddressEditDialogComponent
   */
  @Output()
  form: FormGroup;

  /**
   * Build form method implementation
   *
   * @param {(isCreate: boolean) => void} callback
   * @returns {FormGroup}
   *
   * @memberof NodeBaseAddressEditDialogComponent
   */
  buildForm(callback: (isCreate: boolean) => void): FormGroup {

    this.errors.Address = '';
    this.messages.Address = {
      required: ValidationMessages.hostNameIsRequired,
      pattern: ValidationMessages.hostNameInvalid
    };

    this._address = new Uri(this.data.Address);

    return this.formBuilder.group({
      Address: [this._address.Host, FormValidators.forHostName()]
    });
  }

  /**
   * Creates an instance of NodeBaseAddressEditDialogComponent.
   * @param {NodesStoreService} storeService - nodes store service
   * @param {FormBuilder} formBuilder - form builder
   * @param {DialogRef<NodeBaseAddressEditDialogComponent>} _dialogRef - current dialog reference
   * @param {NodeAddress} data - node address data
   *
   * @memberof NodeBaseAddressEditDialogComponent
   */
  constructor(
    storeService: NodesStoreService,
    formBuilder: FormBuilder,
    private readonly _dialogRef: DialogRef<NodeBaseAddressEditDialogComponent>,
    @Inject(SW_DIALOG_DATA) public data: NodeAddress
  ) {
      super(storeService.store, formBuilder);
      this.buildFormAndResetValidity();
  }

  /**
   * Close event handler
   *
   * @param {boolean} [isCommit=false]
   *
   * @memberof NodeBaseAddressEditDialogComponent
   */
  close() {
    this._address.Host = this.form.value.Address;
    this._dialogRef.close(Object.assign(new NodeAddress(), this.data, {
      Address: this._address.toString()
    }));
  }

}
