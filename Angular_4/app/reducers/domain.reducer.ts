import { ActionReducer } from '@ngrx/store';
import { createSelector } from 'reselect';

import { DomainObjectBase, EntityType } from '../prodapp-core/model';
import { IDictionary, ReadOnlyDictionary, KeyValuePair, ArrayHelper } from '../prodapp-core/utils';

import * as domains from '../actions/domain.actions';

/**
 * Defines an interface for the application state that stores
 * {DomainObjectBase} entities.
 *
 * @export
 * @interface IDomainState
 * @extends {IEntityReducerBase<T>}
 * @template T - the type of domain entities stored in this state.
 */
export interface IDomainState<T extends DomainObjectBase> {

  /**
   * When implemented by a class, gets or sets the dictionary of entities
   * stored in this state.
   *
   * @type {IDictionary<number, T>}
   * @memberof IDomainState
   */
  entities: IDictionary<number, T>;
};

/**
 * Base reducer slice for normalized entities
 *
 * @export
 * @class EntityReducerBase
 * @implements {IEntityReducerBase<T>}
 * @template T
 */
export class EntityReducerBase<T extends DomainObjectBase> implements IDomainState<T> {

  /**
   * Entities in the store slice
   *
   * @type {Dictionary<T>}
   * @memberof IEntityReducerBase
   */
  entities: IDictionary<number, T> = new ReadOnlyDictionary<number, T>();
}

/**
 * Compose a domain reducer from a reducer and domain key.
 *
 * @export
 * @template T
 * @param {ActionReducer<DomainState<T>>} reducer
 * @param {EntityType} domain
 * @returns
 */
export function composeDomain<T extends DomainObjectBase>(reducer: ActionReducer<DomainState<T>>, domain: EntityType) {
  return function (state: any, action: domains.Actions<T>) {
    if (domains.isDomainAction<T>(action)) {
      if (domain === action.payload.domain) {
        const domainState = domainReducer<T>(state, action);
        return reducer(domainState, action);
      }
    }

    return reducer(state, action);
  };
}

/**
 * The default {IDomainState<T>} implementation.
 * 
 * @export
 * @class DomainState
 * @implements {IDomainState<T>}
 * @template T
 */
export class DomainState<T extends DomainObjectBase> implements IDomainState<T> {

  /** @inheritdoc */
  entities: IDictionary<number, T>;

  /**
   * Creates an instance of DomainState.
   *
   * @param {IDictionary<number, T>} [entities] - the optional dictionary of entities
   * with which to initialize this state.
   *
   * @memberof DomainState
   */
  constructor(entities?: IDictionary<number, T>) {
      this.entities = new ReadOnlyDictionary(entities ? entities.items() : null);
  }
}

export function domainReducer<T extends DomainObjectBase>(state: IDomainState<T> = new EntityReducerBase<T>(), action: domains.Actions<T>) {

  switch (action.type) {

    case domains.ActionTypes.LOAD_DOMAIN_ENTITY_SUCCESS:
      const { entity } = action.payload;

      let items: KeyValuePair<number, T>[] = [...state.entities.items()];
      const item: KeyValuePair<number, T> = state.entities.getItem(entity.Id);
      const newItem = new KeyValuePair<number, T>(entity.Id, entity);

      if (!item) {
        items = ArrayHelper.add(items, newItem);
      } else {
        items = ArrayHelper.replaceItem(items, item, newItem);
      }

      return Object.assign({},
        state,
        {
          entities: new ReadOnlyDictionary<number, T>(items)
        });

    default:
      return state;
  }

}

/** Gets the dictionary of entities stored in this state. */
export const entities = <T extends DomainObjectBase>(state: DomainState<T>) => state.entities;

/** Gets a domain object entity by id. */
export function getDomainObject<T extends DomainObjectBase>(id: number) {
  return createSelector(entities, (state) => {
    return state.getValue(id);
  });
}

/** Checks whether a domain object entity with the specified id exists in this state. */
export function hasDomainObject<T extends DomainObjectBase>(id: number) {
  return createSelector(entities, (state) => {
    return state.containsKey(id);
  });
}
