import { Action } from '@ngrx/store';
import { label } from '../prodapp-core/utils/Actions';

import { DomainObjectBase, EntityType } from '../prodapp-core/model';
import { IEntityKeyInfo } from '../prodapp-core/interfaces/IEntityKeyInfo';

/**
 * Action Dictionary -- Typed labels for actions for use in reducers
 */
export class ActionTypes {
  static readonly LOAD_DOMAIN_ENTITY = label('[ Domain ] Load Domain Object');
  static readonly LOAD_DOMAIN_ENTITY_SUCCESS = label('[ Domain ] Load Domain Object Success');
  static readonly LOAD_DOMAIN_ENTITY_FAIL = label('[ Domain ] Load Domain Object Failure');
  static readonly UPDATE_DOMAIN_ENTITY = label('[ Domain ] Update Domain Object');
  static readonly CLONE_DOMAIN_ENTITY = label('[ Domain ] Clone Domain Object');
  static readonly UPDATE_ENTITY_KEY_INFO = label('[ App ] Update Entity Key Info');
};

/**
 * Load a domain Entity
 *
 * @export
 * @class LoadDomainObjectAction
 * @implements {Action}
 * @template T
 */
export class LoadDomainEntityAction<T extends DomainObjectBase> implements Action {
  public readonly type = ActionTypes.LOAD_DOMAIN_ENTITY;
  /**
   * Creates an instance of LoadDomainObjectAction.
   *
   * @param {{ id: number, domain: EntityType }} payload
   *
   * @memberof LoadDomainObjectAction
   */
  constructor(public payload: { id: number, domain: EntityType }) { }
}

/**
 * Load Domain Entity Success
 *
 * @export
 * @class LoadDomainObjectSuccess
 * @implements {Action}
 * @template T
 */
export class LoadDomainEntitySuccess<T extends DomainObjectBase> implements Action {
  public readonly type = ActionTypes.LOAD_DOMAIN_ENTITY_SUCCESS;
  constructor(public payload: { entity: T, domain: EntityType }) { }
}

/**
 * Load Domain Entity Failure
 *
 * @export
 * @class LoadDomainObjectFail
 * @implements {Action}
 */
export class LoadDomainEntityFail implements Action {
  public readonly type = ActionTypes.LOAD_DOMAIN_ENTITY_FAIL;
  /**
   * Creates an instance of LoadDomainObjectFail.
   *
   * @param {{error: Error, domain: EntityType}} payload
   *
   * @memberof LoadDomainObjectFail
   */
  constructor(public payload: {error: Error, domain: EntityType}) { }
}

/**
 * Update Domain Entity
 *
 * @export
 * @class UpdateDomainObject
 * @implements {Action}
 * @template T
 */
export class UpdateDomainEntity<T extends DomainObjectBase> implements Action {

  /**
   * Gets action type.
   */
  public readonly type = ActionTypes.UPDATE_DOMAIN_ENTITY;

  /**
   * Creates an instance of UpdateDomainEntity action.
   *
   * @param {{ object: T, domain: EntityType, withResult?: boolean }} payload - the action payload
   * where object is an entity to create or update, domain is the entity type, and withResult flag
   * indicates whether the updated entity should be returned or just its id.
   *
   * @memberof UpdateDomainObject
   */
  constructor(public payload: { entity: T, domain: EntityType, withResult?: boolean }) { }
}

/**
 * Clone Domain Entity
 *
 * @export
 * @class UpdateDomainObject
 * @implements {Action}
 * @template T
 */
export class CloneDomainEntity<T extends DomainObjectBase> implements Action {

  /**
   * Action type.
   */
  public readonly type = ActionTypes.CLONE_DOMAIN_ENTITY;

  /**
   * Creates an instance of CloneDomainEntity action.
   *
   * @param {{ object: T, domain: EntityType, withResult?: boolean }} payload - the action payload
   * where object is an entity to clone, domain is the entity type, and withResult flag
   * indicates whether the cloned entity should be returned or just its id.
   *
   * @memberof CloneDomainEntity
   */
  constructor(public payload: { entity: T, domain: EntityType, withResult?: boolean }) { }
}

/**
 * Update entity key info action
 *
 * @export
 * @class UpdateEntityKeyInfoAction
 * @implements {Action}
 * @template T - updated entity type
 */
export class UpdateEntityKeyInfoAction<T> implements Action {
  /**
   * Action type
   *
   * @memberof UpdateEntityKeyInfoAction
   */
  public readonly type = ActionTypes.UPDATE_ENTITY_KEY_INFO;

  /**
   * Creates an instance of UpdateEntityKeyInfoAction.
   * @param {{domain: EntityType, entity: T, keyInfo: IEntityKeyInfo }} payload  action payload
   * @memberof UpdateEntityKeyInfoAction
   */
  constructor(public payload: {domain: EntityType, entity: T, keyInfo: IEntityKeyInfo }) {}
}

/**
 * Type Guard for Domain Actions
 *
 * @export
 * @template T
 * @param {Action} action
 * @returns {action is Actions<T>}
 */
export function isDomainAction<T>(action: Action): action is Actions<T> {
  return action instanceof LoadDomainEntityAction
    || action instanceof LoadDomainEntitySuccess
    || action instanceof LoadDomainEntityFail
    || action instanceof UpdateDomainEntity;
}

export type Actions<T extends DomainObjectBase> =
  LoadDomainEntityAction<T>
  | LoadDomainEntitySuccess<T>
  | LoadDomainEntityFail
  | UpdateDomainEntity<T>
  | CloneDomainEntity<T>
  | UpdateEntityKeyInfoAction<T>;
