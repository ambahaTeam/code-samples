import * as moment from 'moment';

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { NodeDefinition,
  NodeDefinitionUI,
  NodeAddressSetUI,
  NodeAddress,
  NodeInstance,
  X509CertificateIdentityDefinitionBase,
  UserDefinition,
  Issuer,
  NodeSettings,
  EntityType,
  EntityKeyInfoWithName,
  IdentityKind,
  NodeAddressKind,
  KeyInfo
} from '../../prodapp-core/model';

import { FormValidators , ValidationMessages } from '../../shared/validation';
import { EditComponentBase } from '../../shared/components';
import { IdentityViewDialogComponent } from '../../shared/identity-view-dialog/identity-view-dialog.component';

import { NodesService} from '../../core/services';
import { NodesStoreService } from '../../core/store/nodes-store.service';
import { UsersStoreService } from '../../core/store/users-store.service';
import { DialogService } from '../../shared/dialog/dialog.service';
import { NodeBaseAddressEditDialogComponent } from '../node-base-address-edit-dialog/node-base-address-edit-dialog.component';

/**
* A component that handles the {NodeDefinition} edit page logic.
*/
@Component({
  selector: 'sw-node-summary-edit',
  templateUrl: './node-summary-edit.component.html',
  styleUrls: ['./node-summary-edit.component.scss']
})
export class NodeSummaryEditComponent extends EditComponentBase<NodeDefinitionUI> {

  private _keyValid = true;
  private _advancedDataValid = true;
  private _canSave = false;

  /**
   * Gets a flag that enables or disables Save action button.
   *
   * @type {boolean}
   * @memberof NodeEditComponent
   */
  get canSave(): boolean {
    return this._canSave && this.form.valid && this._keyValid && this._advancedDataValid;
  }

  /**
   * Sets a flag that enables or disables Save action button.
   *
   * @type {boolean}
   * @memberof NodeEditComponent
   */
  @Input()
  set canSave(value: boolean) {
    this._canSave = value;
  }

  /**
   * Gets or sets a flag that enables or disables Refresh action button.
   *
   * @type {boolean}
   * @memberof NodeEditComponent
   */
  @Input()
  canRefresh = true;

  /**
   * Node definition for UI
   *
   * @type {NodeDefinitionUI}
   * @memberof NodeSummaryEditComponent
   */
  @Input()
  data: NodeDefinitionUI;

  /**
   * Current user data
   *
   * @type {UserDefinition}
   * @memberof NodeSummaryEditComponent
   */
  @Input()
  user: UserDefinition;

  /**
   * Event emitted when Save action button is clicked.
   *
   * @memberof NodeEditComponent
   */
  @Output()
  commit = new EventEmitter<NodeDefinitionUI>();

  /**
   * Event emitted when Refresh action button is clicked.
   *
   * @memberof BindingEditComponent
   */
  @Output()
  refresh = new EventEmitter<number>();

  /**
   * Event emitted when Delete action button is clicked.
   */
  @Output()
  delete = new EventEmitter<NodeDefinition>();

  /**
   * X509 certificate identity kind
   *
   * @memberof NodeSummaryEditComponent
   */
  readonly certificateIdentityKind = IdentityKind.X509Certificate;

  /**
   * Base addresses filtered by address kind
   *
   * @type {NodeAddressSetUI[]}
   * @memberof NodeSummaryEditComponent
   */
  baseAddressesFiltered: NodeAddressSetUI[];

  /**
   * Returns true if empty node was just created, but not saved yet
   *
   * @memberof NodeSummaryEditComponent
   */
  isNew = () => this.data && this.data.Id <= 0;

  /**
   * Creates an instance of NodeSummaryEditComponent.
   * @param {NodesService} _service - nodes service
   * @param {NodesStoreService} _nodesStoreService - nodes store service
   * @param {UsersStoreService} _usersStoreService - users store service
   * @param {DialogService} _dialogService - dialog service
   * @param {FormBuilder} formBuilder - form builder
   * @memberof NodeSummaryEditComponent
   */
  constructor(
    private readonly _service: NodesService,
    private readonly _nodesStoreService: NodesStoreService,
    private readonly _usersStoreService: UsersStoreService,
    private readonly _dialogService: DialogService,
    formBuilder: FormBuilder
  ) {
    super(_nodesStoreService.store, formBuilder);

    this.errors.Name = '';
    this.errors.HeartbeatInterval = '';
    this.errors.HeartbeatIntervalSeconds = '';

    this.messages.Name = {
      required: ValidationMessages.nameRequired,
      pattern: ValidationMessages.nameInvalid
    };

    this.messages.HeartbeatInterval = {
      required: ValidationMessages.valueRequired,
      pattern: ValidationMessages.integerInvalid
    };

    this.messages.HeartbeatIntervalSeconds = {
      required: ValidationMessages.valueRequired,
      pattern: ValidationMessages.integerInvalid
    };
  }

  /**
  * Builds an HTML form for editing {NodeDefinition} data.
  */
  buildForm(callback: (isCreate: boolean) => void) {
    callback(this.isNew());
    this.baseAddressesFiltered = this.getBaseAddressSets();
    return this.formBuilder.group({
      IsEnabled: [this.data.IsEnabled],
      HeartbeatInterval: [this.data.HeartbeatInterval],
      HeartbeatIntervalSeconds: [this.data.HeartbeatInterval.asSeconds(), FormValidators.forWholeNumber()],
      LastUpdate: [this.data.LastUpdate, FormValidators.forDate()],
      NodeType: [this.data.NodeType],
      Instances: [this.data.Instances],
      BaseAddresses: [this.data.BaseAddresses],
      Identities: [this.data.Identities],
      TrustedIssuers: [this.data.TrustedIssuers]
    });
  }

  /**
   * Handles node refresh event.
   *
   * @memberof NodeEditComponent
   */
  onRefresh() {
    this.refresh.emit(this.data.Id);
  }

 /**
   * Handles Delete action by emitting delete event.
   *
   * @memberof BindingEditComponent
 */
  onDelete() {
    this.delete.emit(this.data);
  }

  /**
   * Saves the node
   *
   * @memberof NodeSummaryEditComponent
   */
  onSave() {
    const node = Object.assign(new NodeDefinition(), this.data, this.form.value);
    node.HeartbeatInterval = moment.duration(+this.form.value.HeartbeatIntervalSeconds, 'seconds');
    this._nodesStoreService.save(node, true);
  }

  /**
   * Handles change of key value
   *
   * @param {KeyInfo} key - key data
   *
   * @memberof NodeSummaryEditComponent
   */
  onKeyInfoChanged(key: KeyInfo) {
    this._keyValid = key.Valid;
    if (this.canSave) {
      const entity = Object.assign(new NodeDefinition(), this.data, this.form.value);
      this._nodesStoreService.updateKeyInfo(EntityType.Node, {
        KeyInfo: new EntityKeyInfoWithName(key.Key, key.Name),
        Entity: entity
      });
    }
  }

  /**
   * Get array with base address sets filtered by address kind
   *
   * @returns {NodeAddressSetUI[]}
   * @memberof NodeSummaryEditComponent
   */
  getBaseAddressSets(): NodeAddressSetUI[] {
    return this.data.BaseAddresses ? this._service.getBaseAddressSets(this.data.BaseAddresses) : [];
  }

  /**
   * Get display names for node configuration change modes
   *
   * @returns
   *
   * @memberof NodeSummaryEditComponent
   */
  getConfigurationChangeModes() {
    return this._service.getDisplayNamesForNodeConfigurationChangeModes();
  }

  /**
   * Node base address updated handler
   *
   * @param {NodeAddress} nodeAddress - node address to edit
   *
   * @memberof NodeSummaryEditComponent
   */
  onBaseAddressUpdate(nodeAddress: NodeAddress) {
    switch (nodeAddress.Kind) {
      case NodeAddressKind.Physical:
        this.openPhysicalAddressEditDialog(nodeAddress);
      break;
      case NodeAddressKind.AzureServiceBus:
        this.updateBaseAddress(nodeAddress);
      break;
    }
  }

  /**
   * Fires when dialog for node address edit was closed
   *
   *
   * @memberof NodeSummaryEditComponent
   */
  onBaseAddressEditDialogClose = (entity: NodeAddress) =>  {
    if (entity) {
      this.updateBaseAddress(entity);
    }
  }

  /**
   * Event handler for node base address deletion
   *
   * @param {number} nodeAddressId - node base address id
   *
   * @memberof NodeSummaryEditComponent
   */
  onBaseAddressDelete(nodeAddressId: number) {
    this._nodesStoreService.deleteBaseAddress(Object.assign(new NodeDefinition(), this.data), nodeAddressId);
    this.baseAddressesFiltered = this.getBaseAddressSets();
  }

  /**
   * Event handler for node instance deletion
   *
   * @param {NodeInstance} instance - node instance for deletion
   *
   * @memberof NodeSummaryEditComponent
   */
  onNodeInstanceDelete(instance: NodeInstance) {
    this._nodesStoreService.deleteInstance(Object.assign(new NodeDefinition(), this.data), instance.Id);
  }

  /**
   * Node identity view event handler (shows dialog with certificate info)
   *
   * @param {X509CertificateIdentityDefinitionBase} identity
   *
   * @memberof NodeSummaryEditComponent
   */
  onNodeIdentityView(identity: X509CertificateIdentityDefinitionBase) {
    this._usersStoreService.getCertificate(new UserDefinition(), identity);
    this._dialogService.open<IdentityViewDialogComponent, X509CertificateIdentityDefinitionBase>(IdentityViewDialogComponent, {
      data: identity
    });
  }

  /**
   * Trusted issuer update event handler
   *
   * @param {Issuer} issuer
   *
   * @memberof NodeSummaryEditComponent
   */
  onTrustedIssuerUpdate(issuer: Issuer) {
    this._nodesStoreService.updateIssuer(Object.assign(new NodeDefinition(), this.data), issuer);
  }

  /**
   * Trusted issuer delete event handler
   *
   * @param {Issuer} issuer - trusted named issuer
   *
   * @memberof NodeSummaryEditComponent
   */
  onTrustedIssuerDelete(issuer: Issuer) {
    this._nodesStoreService.deleteIssuer(Object.assign(new NodeDefinition(), this.data), issuer.Id);
  }

  /**
   * Advanced data commit changes event handler
   *
   * @param {{data: NodeSettings, isValid: boolean}} event - custom event with settings and validation flag
   *
   * @memberof NodeSummaryEditComponent
   */
  onAdvancedDataCommit(event: {data: NodeSettings, isValid: boolean}) {
    if (event.isValid) {
      const node = Object.assign(new NodeDefinition(), this.data, this.form.value);
      this._nodesStoreService.updateSettings(node, event.data);
    }
    this._advancedDataValid = event.isValid;
  }

  /**
   * Opens dialog for physical node address edit
   *
   * @private
   * @param {NodeAddress} address - node address
   * @memberof NodeSummaryEditComponent
   */
  private openPhysicalAddressEditDialog(address: NodeAddress) {
    this._dialogService.open<NodeBaseAddressEditDialogComponent, NodeAddress>(NodeBaseAddressEditDialogComponent, {
      data: address
    }).afterClosed().subscribe(this.onBaseAddressEditDialogClose);
  }

  /**
   * Calls nodes store service for update base address action and refreshes filtered base addresses for the UI
   *
   * @private
   * @param {NodeAddress} address
   * @memberof NodeSummaryEditComponent
   */
  private updateBaseAddress(address: NodeAddress) {
    this._nodesStoreService.updateBaseAddress(Object.assign(new NodeDefinitionUI(), this.data),
                                              Object.assign(new NodeAddress(), address));
    this.baseAddressesFiltered = this.getBaseAddressSets();
  }
}
