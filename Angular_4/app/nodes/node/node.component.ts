import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from '../../reducers';
import { SelectNodeAction } from '../../actions/selected.actions';
import { NEW_ID } from '../../prodapp-core/constants';
import { NodesStoreService } from '../../core/store';

@Component({
  selector: 'sw-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnDestroy {

  /** Gets the observable for the currently selected entity */
  public readonly node$ = this._nodesStoreService.getSelected();

  /**
   * Subscribes to the router and selects a node.
   * This prevents router subscription from propagating downstream.
   *
   * @type {Subscription}
   * @memberof NodeComponent
   */
  actionsSubscription: Subscription;

  /**
   * Initializes the new instance of this component.
   * @param {Store<AppState>} store - the app store that provides access to entity data.
   * @param {ActivatedRoute} route - the route that contains routing info, such as entity id (key).
   * @param {NodesStoreService} _nodesStoreService - the nodes store service.
   */
  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private readonly _nodesStoreService: NodesStoreService
  ) {
    this.actionsSubscription = route.params
      .select<string>('id')
      .map(id => new SelectNodeAction(Number.isNaN(+id) ? NEW_ID : +id))
      .subscribe(store);
  }


  /**
   * Disposes of this component.
   *
   *
   * @memberof NodeComponent
   */
  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }

}
