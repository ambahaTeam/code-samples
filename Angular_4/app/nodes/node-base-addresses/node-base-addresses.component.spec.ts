import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeBaseAddressesComponent } from './node-base-addresses.component';

describe('NodeBaseAddressesComponent', () => {
  let component: NodeBaseAddressesComponent;
  let fixture: ComponentFixture<NodeBaseAddressesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeBaseAddressesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeBaseAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
