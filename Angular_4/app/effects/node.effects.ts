import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import { EntityEffectsBase } from './base-entity.effects';

import { NodesService } from '../core/services';
import { FolderEntityTypes, EntityType, NodeDefinition } from '../prodapp-core/model';
import { RepositoryNode } from '../prodapp-core/repository';

import * as repository from '../actions/repository.actions';
import * as nodeActions from '../actions/node.actions';
import * as errorActions from '../actions/error.actions';
import * as appActions from '../actions/app.actions';


@Injectable()
export class NodeEffects extends EntityEffectsBase<number, NodeDefinition> {

  /**
   * Defines the node definition load stream of store actions.
   *
   * @memberof NodeEffects
   */
  @Effect() public readonly loadNode$ = this.createLoadEntityStream();

  /**
   * Defines the update stream for node key info
   *
   * @memberof NodeEffects
   */
  @Effect() public readonly updateKeyInfo$ = this.createEntityUpdateKeyInfoStream();

  /**
   * Defines the node definition update stream of store actions.
   *
   * @memberof NodeEffects
   */
  @Effect() updateNode$ = this.createEntityUpdateStream((nodeDefinition) => {
      const node = new RepositoryNode(nodeDefinition.Id, FolderEntityTypes.Node, false, nodeDefinition.Name);
      node.parentId =  nodeDefinition.FolderId ? nodeDefinition.FolderId : 0;
      return [
        new repository.UpdateNodeAction(node),
        new nodeActions.LoadNodeSuccessAction(nodeDefinition),
        new appActions.ClearPendingChangesAction()
      ];
  });

  ...

   /**
   * An implementation that subscribes to {GetNodeEndpointsAction}
   * and updates {EndpointSummary[]} when an interaction that updates node endpoints
   * is detected.
    * @memberof NodeEffects
    */
   @Effect() loadNodeEndpoints$ = this.actions$
      .ofType(nodeActions.ActionTypes.GET_NODE_ENDPOINTS)
      .map((action: nodeActions.GetNodeEndpointsAction) => action.payload)
      .switchMap(({nodeId}) => this.includeProcessing(
        this.service.getNodeEndpoints(nodeId)
        .map((endpoints) => new nodeActions.GetNodeEndpointsSuccessAction({endpoints}))
        .catch(error => Observable.of(new errorActions.ErrorAction({ error })))));

   /**
   * An implementation that subscribes to {GetNodeActivitiesAction}
   * and updates {NodeActivity[]} when an interaction that updates node activities
   * is detected.
    * @memberof NodeEffects
    */
   @Effect() loadNodeActivityLogs$ = this.actions$
      .ofType(nodeActions.ActionTypes.GET_NODE_ACTIVITY_LOGS)
      .map((action: nodeActions.GetNodeActivitiesAction) => action.payload)
      .switchMap(({request}) => this.includeProcessing(
        this.service.getNodeActivities(request)
        .map((activities) => new nodeActions.GetNodeActivitiesSuccessAction({ request, activities }))
        .catch(error => Observable.of(new errorActions.ErrorAction({ error })))));

    /**
   * Creates an instance of {NodeEffects}.
   *
   * @param {Actions} actions$: Observable stream of all actions
   * @param {NodeService} api
   *
   * @memberof NodeEffects
   */
  constructor(
    actions$: Actions,
    protected service: NodesService
  ) {
    super(actions$, service, EntityType.Node);
  }
}
