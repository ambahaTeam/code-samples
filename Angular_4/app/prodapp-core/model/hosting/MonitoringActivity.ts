import { Severity } from '..';
import { Moment } from 'moment';

/**
 * Represents a single monitoring MonitoringActivity.
 *
 * @export
 * @class MonitoringActivity
 */
export class MonitoringActivity {

    /**
     * Gets or sets this domain object's id.
     * @type {number}
     * @memberof MonitoringActivity
     */
    Id: number;

    /**
     * Gets this object's created date and time.
     * @type {Moment}
     * @memberof MonitoringActivity
     */
    Created: Moment;

    /**
     * Gets this object's updated date and time.
     * @type {Moment}
     * @memberof MonitoringActivity
     */
    Updated: Moment;

    /**
     * Gets or sets the node id.
     * @type {number}
     * @memberof MonitoringActivity
     */
    NodeId: number;

    /**
     * Gets the node friendly name.
     * @type {string}
     * @memberof MonitoringActivity
     */
    NodeName: string;

    /**
     * Gets or sets identifier of the node instance that reported this MonitoringActivity.
     * @type {number}
     * @memberof MonitoringActivity
     */
    InId: number;

    /**
     * Gets the node instance name.
     * @type {string}
     * @memberof MonitoringActivity
     */
    NodeInstanceName: string;

    /**
     * Gets or sets the transaction id (optional).
     * @type {string}
     * @memberof MonitoringActivity
     */
    TranId: string;

    /**
     * Gets or sets unique message exchange identifier (optional).
     * @type {string}
     * @memberof MonitoringActivity
     */
    ExchId: string;

    /**
     * Gets or sets the service version id (optional).
     * @type {number}
     * @memberof MonitoringActivity
     */
    SvcId: number;

    /**
     * Gets the service version friendly name.
     * @type {string}
     * @memberof MonitoringActivity
     */
    ServiceVersionName: string;

    /**
     * Gets or sets the endpoint id (optional).
     * @type {number}
     * @memberof MonitoringActivity
     */
    EpId: number;

    /**
     * Gets the endpoint name.
     * @type {string}
     * @memberof MonitoringActivity
     */
    EndpointName: string;

    /**
     * Gets or sets the monitoring MonitoringActivity date and time in UTC.
     * @type {Moment}
     * @memberof MonitoringActivity
     */
    MonitoringActivityDateTime: Moment;

    /**
     * Gets or sets the monitoring MonitoringActivity severity level.
     * @type {Severity}
     * @memberof MonitoringActivity
     */
    Severity: Severity;

    /**
     * Gets or sets the MonitoringActivity message.
     * @type {string}
     * @memberof MonitoringActivity
     */
    Message: string;

    /**
     * Gets or sets the MonitoringActivity details (optional).
     * @type {string}
     * @memberof MonitoringActivity
     */
    Details: string;
}
