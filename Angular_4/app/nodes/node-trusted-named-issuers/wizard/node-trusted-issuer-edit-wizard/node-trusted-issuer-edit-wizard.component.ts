import { Component, ViewChild, Inject, OnDestroy, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import {
  WizardClosingState,
  Issuer,
  X509CertificateIdentityDefinition,
  UserDefinition,
  X509CertificateFederationIdentityDefinition,
  CertificateInfo
} from '../../../../prodapp-core/model';
import { UsersStoreService } from '../../../../core/store';
import { ADD, MODIFY } from '../../../../prodapp-core/constants';

import { EditComponentBase } from '../../../../shared/components';
import { ValidationMessages, FormValidators } from '../../../../shared/validation';
import { DialogRef, SW_DIALOG_DATA } from '../../../../shared/dialog';
import { uuid4 } from '../../../../prodapp-core/utils';

// tslint:disable-next-line:max-line-length
import { IdentityX509CertificateStepComponent } from '../../../../shared/identities-grid/identity-edit-wizard/steps/identity-x509-certificate-step/identity-x509-certificate-step.component';
// tslint:disable-next-line:max-line-length
import { IdentityX509CertificateViewStepComponent } from '../../../../shared/identities-grid/identity-edit-wizard/steps/identity-x509-certificate-view-step/identity-x509-certificate-view-step.component';

/**
 * Data for trusted named issuers wizard
 *
 * @export
 * @interface INodeTrustedIssuerEditWizardData
 */
export interface INodeTrustedIssuerEditWizardData {

  /**
   * Issuer data
   *
   * @type {Issuer}
   * @memberof INodeTrustedIssuerEditWizardData
   */
  issuer: Issuer;

  /**
   * Current user data
   *
   * @type {UserDefinition}
   * @memberof INodeTrustedIssuerEditWizardData
   */
  user: UserDefinition;
}

/**
 * Wizard for edit of node trusted named issuers
 *
 * @export
 * @class NodeTrustedIssuerEditWizardComponent
 * @extends {EditComponentBase<Issuer>}
 * @implements {OnDestroy}
 */
@Component({
  selector: 'sw-node-trusted-issuer-edit-wizard',
  templateUrl: './node-trusted-issuer-edit-wizard.component.html',
  styleUrls: ['./node-trusted-issuer-edit-wizard.component.scss'],
})
export class NodeTrustedIssuerEditWizardComponent extends EditComponentBase<Issuer> implements OnDestroy {

  private _certificateSub: Subscription;

  /**
   * Certificate edit step
   *
   * @type {IdentityX509CertificateStepComponent}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  @ViewChild(IdentityX509CertificateStepComponent)
  certificateStep: IdentityX509CertificateStepComponent;

  /**
   * Certificate view step
   *
   * @type {IdentityX509CertificateViewStepComponent}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  @ViewChild(IdentityX509CertificateViewStepComponent)
  certificateViewStep: IdentityX509CertificateViewStepComponent;

  /**
   * Issuer data
   *
   * @type {Issuer}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  data: Issuer;

  /**
   * Commit event
   *
   * @type {EventEmitter<Issuer>}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  commit: EventEmitter<Issuer>;

  /**
   * Wizard title
   *
   * @type {string}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  title: string;

  /**
   * Flag which indicates that certificate data is valid
   *
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  certificateDataIsValid = false;

  /**
   * Default X.509 certificate identity
   *
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  readonly identity = new X509CertificateIdentityDefinition();

  /**
   * Observable which indicates that wizard can be closed
   *
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  readonly canClose$ = Observable.of(this.canFinish() ? WizardClosingState.OkStayOpen : WizardClosingState.FailedStayOpen);

  /**
   * Uploaded certificate data
   *
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  readonly certificate$ = this._usersStore.getSelectedCertificate();

  /**
   * Creates an instance of NodeTrustedIssuerEditWizardComponent.
   * @param {FormBuilder} formBuilder - form builder
   * @param {DialogRef<NodeTrustedIssuerEditWizardComponent>} _dialogRef - dialog reference
   * @param {UsersStoreService} _usersStore - users store service
   * @param {INodeTrustedIssuerEditWizardData} data - injected data
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  constructor(formBuilder: FormBuilder,
              private readonly _dialogRef: DialogRef<NodeTrustedIssuerEditWizardComponent>,
              private readonly _usersStore: UsersStoreService,
              @Inject(SW_DIALOG_DATA) public readonly wizardData: INodeTrustedIssuerEditWizardData) {
    super(_usersStore.store, formBuilder);
    this.data = wizardData.issuer;
    this.buildFormAndResetValidity();
    this.title = `${this.data.Id < 0 && !this.data.IssuerName ? ADD : MODIFY} Trusted Issuer Identity`;
    if (this.data.Certificate) {
      const certificateDefinition = new X509CertificateIdentityDefinition();
      certificateDefinition.RawData = this.data.Certificate;
      _usersStore.getCertificate(null, certificateDefinition);
    }
  }

  /**
   * Implementation of destroy life-cycle event hook
   *
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  ngOnDestroy(): void {
    this.unsubscribe();
  }

  /**
   * Builds form
   *
   * @param {(isCreate: boolean) => void} callback
   * @returns {FormGroup}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  buildForm(callback: (isCreate: boolean) => void): FormGroup {

    this.errors.IssuerName = '';

    this.messages.IssuerName = {
      required: ValidationMessages.nameRequired
    }

    return this.formBuilder.group({
      IssuerName: [this.data.IssuerName, FormValidators.forRequired()]
    });
  }

  /**
   * Returns true if wizard can finish it's execution
   *
   * @returns {boolean}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  canFinish(): boolean {
    return this.certificateDataIsValid;
  }

  /**
   * Wizard finish event handler
   *
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  onFinish() {
    this.processX509Certificate();
  }

  /**
   * Unsubscribes from internal subscription
   *
   * @private
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  private unsubscribe() {
    if (this._certificateSub) {
      this._certificateSub.unsubscribe();
    }
  }

  /**
   * Processes X.509 certificate data
   *
   * @private
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  private processX509Certificate() {
    const info = this.certificateStep.getInfo();
    const viewInfo = this.certificateViewStep ? this.certificateViewStep.getInfo() : null;
    if (viewInfo) {
      info.identity = viewInfo;
    }
    if (info.needToUploadCertificate) {
      // Close dialog if identity wasn't changed
      if (!info.identity.RawData) {
        this.closeWizardWithIssuer(viewInfo, this.certificateViewStep.data);
        return;
      }
      this.certificateDataIsValid = false;
      this.unsubscribe();
      this._certificateSub = this.certificate$.subscribe(certificate => {
        if (certificate && certificate.Thumbprint) {
          this.certificateDataIsValid = true;
          this.closeWizardWithIssuer(viewInfo, certificate);
        }
      });
      this._usersStore.getCertificate(null, info.identity, info.password);
    } else {
      this.certificateDataIsValid = true;
      const issuer = this.createIssuer();
      issuer.Thumbprint = info.thumbprint;
      issuer.FriendlyName = `Thumbprint: ${info.thumbprint}`;
      this._dialogRef.close(issuer);
    }
  }

  /**
   * Closes wizard dialog with issuer
   *
   * @private
   * @param {X509CertificateFederationIdentityDefinition} viewInfo - certificate view info
   * @param {CertificateInfo} certificate - certificate data
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  private closeWizardWithIssuer(viewInfo: X509CertificateFederationIdentityDefinition, certificate: CertificateInfo) {
    const issuer = this.createIssuer();
    issuer.Certificate = viewInfo ? viewInfo.RawData : certificate.RawData;
    issuer.FriendlyName = certificate.toString();
    issuer.Thumbprint = certificate.Thumbprint;
    this._dialogRef.close(issuer);
  }

  /**
   * Creates empty issuer
   *
   * @private
   * @returns {Issuer}
   * @memberof NodeTrustedIssuerEditWizardComponent
   */
  private createIssuer(): Issuer {
    const issuer = new Issuer();
    issuer.Key = uuid4();
    issuer.Id = this.data.Id;
    issuer.IssuerName = this.form.value.IssuerName;
    return issuer;
  }
}
