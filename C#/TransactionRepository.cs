﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LearnLms.BusinessLogic.Models.Course;
using LearnLms.BusinessLogic.Models.Transaction;
using LearnLms.DB;
using LearnLms.DB.Entities;
using LearnLms.Domain.Models.Common;
using LearnLms.Domain.Models.Course;
using LearnLms.Domain.Models.Transaction;
using LearnLms.Domain.Repositories.Transaction;
using Microsoft.EntityFrameworkCore;

namespace LearnLms.BusinessLogic.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly LmsDbContext _context;

        public TransactionRepository(LmsDbContext context)
        {
            _context = context;
        }

        public bool ValidateCoursesBeforePayment(long[] courseIds, Guid userId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            var userCourses =
                from t in _context.Transactions.Include(t => t.Orders).SelectMany(t => t.Orders)
                join course in _context.Courses on t.CourseId equals course.CourseId
                join cId in courseIds on course.CourseId equals cId
                where t.UserId == userId
                select new {Transaction = t, Course = course};

            if (user != null && userCourses.Any())
            {
                var sb = new StringBuilder();
                foreach (var userCourse in userCourses)
                    sb.AppendLine(string.Format("User '{0}' already had bought course with name: '{1}'.",
                        user.Email,
                        userCourse.Course.Name));
                throw new Exception(sb.ToString());
            }
            return true;
        }

        public ITransaction AddTransactionWithCourses(string externalTransactionId, decimal amount, long[] courseIds, Guid userId)
        {
            var transaction = _context.Transactions.Add(new Transaction
            {
                UserId = userId,
                DateSettled = DateTime.UtcNow,
                Amount = amount,
                ExternalTransactionId = externalTransactionId
            });

            var boughtCourses = from course in _context.Courses
                join cId in courseIds on course.CourseId equals cId
                select course;

            foreach (var boughtCourse in boughtCourses)
                _context.Orders.Add(new Order
                {
                    CourseId = boughtCourse.CourseId,
                    UserId = userId,
                    Transaction = transaction.Entity
                });

            _context.SaveChanges();

            return new TransactionModel
            {
                Id = transaction.Entity.TransactionId
            };
        }

        public List<ITransaction> GetUserTransactions(Guid id)
        {
            var transactions =
                _context.Transactions.Include(t => t.Orders).Include("Orders.Course").Where(t => t.UserId == id);

            return (from t in transactions
                select new TransactionModel
                {
                    Amount = t.Amount,
                    DateSettled = t.DateSettled,
                    Courses = from order in (from o in t.Orders select o)
                        select new CourseModel
                        {
                            Id = order.Course.CourseId,
                            Name = order.Course.Name
                        } as ICourse
                } as ITransaction).ToList();
        }
    }
}