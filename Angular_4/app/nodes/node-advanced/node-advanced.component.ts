import { Component, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { NodeSettings, ConfigurationChangeMode, ClientAddressOptions } from '../../prodapp-core/model';
import { FormErrors } from '../../shared/validation/form-types';
import { EditComponentBase } from '../../shared/components/base-edit.component';
import { FormValidators } from '../../shared/validation/form-validators';
import { ValidationMessages } from '../../shared/validation/messages';
import { NodesStoreService } from '../../core/store';

/**
 *  Maps values of ClientAddressOptions enumeration to their respective
 *  display names for UI data binding purposes.
 */
export const ClientAddressOptionsMap = {
  ['']: 'First',
  [ClientAddressOptions.TakeLast]: 'Last'
}

/**
 * Component for node advanced data
 *
 * @export
 * @class NodeAdvancedComponent
 */
@Component({
  selector: 'sw-node-advanced',
  templateUrl: './node-advanced.component.html',
  styleUrls: ['./node-advanced.component.scss']
})
export class NodeAdvancedComponent extends EditComponentBase<NodeSettings> {

  /**
   * Commit data event emitter
   *
   * @type {EventEmitter<any>}
   * @memberof NodeAdvancedComponent
   */
  @Output()
  commit: EventEmitter<any> = new EventEmitter<any>();

  /**
   * The the form to bind to
   *
   * @type {FormGroup}
   * @memberof NodeAdvancedComponent
   */
  @Input()
  form: FormGroup;

    /**
   * The errors object
   *
   * @type {*}
   * @memberof NodeSummaryEditComponent
   */
  @Input()
  errors: FormErrors<NodeSettings>;

  /**
   * Gets or sets node settings this component is bound to.
   *
   * @type {NodeSettings}
   * @memberof NodeAdvancedComponent
   */
  @Input()
  data: NodeSettings;

  /**
   * Change modes for node advanced settings
   *
   * @type {{ [key: number]: string }}
   * @memberof NodeAdvancedComponent
   */
  @Input()
  changeModes: { [key: number]: string };

  /**
   * Gets or sets readonly setting for component
   *
   *
   * @memberof NodeSummaryEditFormComponent
   */
  @Input()
  readonly = false;

  /**
   * Indicator of collapsible menu state (opened, closed)
   *
   * @type {boolean}
   * @memberof NodeAdvancedComponent
   */
  open: boolean;

  /**
   * Available client address options
   *
   * @memberof NodeAdvancedComponent
   */
  addressOptions = ClientAddressOptionsMap;

  /**
   * Address postfix text
   *
   * @memberof NodeAdvancedComponent
   */
  readonly addressPostfix = 'IP address if header contains list of IP addresses)';

  /**
   * Creates an instance of NodeAdvancedComponent.
   * @param {NodesStoreService} service - nodes store service
   * @param {FormBuilder} formBuilder - form builder
   * @memberof NodeAdvancedComponent
   */
  constructor(
    service: NodesStoreService,
    formBuilder: FormBuilder
  ) {
    super(service.store, formBuilder);

    this.errors.ChangeMode = '';
    this.errors.ClientAddressHeaderName = '';

    this.messages.ClientAddressHeaderName = {
      pattern: ValidationMessages.headerNameInvalid
    };
  }

  /**
   * Builds form for current component
   *
   * @param {(isCreate: boolean) => void} callback
   * @returns {FormGroup}
   *
   * @memberof NodeAdvancedComponent
   */
  buildForm(callback: (isCreate: boolean) => void): FormGroup {
    return this.formBuilder.group({
      ChangeMode: [this.data.ChangeMode ? this.data.ChangeMode : ConfigurationChangeMode.ServiceRestart],
      ClientAddressHeaderName: [this.data.ClientAddressHeaderName, FormValidators.forHttpHeaderName()],
      ClientAddressOptions: [this.data.ClientAddressOptions !== undefined ? this.data.ClientAddressOptions : '']
    });
  }

  /**
   * Change event handler for form drop-downs
   *
   * @memberof NodeAdvancedComponent
   */
  changed() {
    this.updateFormValidity();
    let settings = new NodeSettings();
    if (this.form.valid) {
      settings = Object.assign(settings, this.form.value);
      if (this.form.value.ClientAddressOptions === '' || this.form.value.ClientAddressHeaderName === '') {
        delete settings.ClientAddressOptions;
      }
    }
    this.commit.emit({data: settings, isValid: this.form.valid});
  }

}
