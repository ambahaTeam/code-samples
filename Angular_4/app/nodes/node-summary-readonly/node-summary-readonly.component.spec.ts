import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NodeSummaryReadonlyComponent } from './node-summary-readonly.component';

describe('NodeSummaryReadonlyComponent', () => {
  let component: NodeSummaryReadonlyComponent;
  let fixture: ComponentFixture<NodeSummaryReadonlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeSummaryReadonlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeSummaryReadonlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
