import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AppState } from '../../../reducers';

import { FormValidators, ValidationMessages } from '../../../shared/validation';
import { EditComponentBase } from '../../../shared/components';
import { KeyInfo } from '../../../prodapp-core/model/KeyInfo';


/**
* A component that handles the {KeyInfo} edit.
*/
@Component({
  selector: 'sw-key-edit-form',
  styleUrls: ['./key-edit-form.component.scss'],
  templateUrl: './key-edit-form.component.html'
})
export class KeyEditFormComponent extends EditComponentBase<KeyInfo> {

  /**
   * The form to bind to
   *
   * @type {FormGroup}
   * @memberof KeyEditorComponent
   */
  @Input()
  form: FormGroup;

  /**
   * Sets the modal dialog state.
   * True to display the dialog, false otherwise.
   *
   * @type {boolean}
   * @memberof IdentityEditComponent
   */
  @Input() open: boolean;

  /**
   * Sets the dialog header.
   *
   * @type {string}
   * @memberof IdentityEditComponent
   */
  @Input() header: string;

  /**
   * {DomainObjectBase} instance this form is bound to.
   *
   * @type {DomainObjectBase}
   * @memberof IdentityEditComponent
   */
  @Input()
  public data: KeyInfo;

  /**
   * Fired when user clicks Save button.
   *
   * @memberof IdentityEditComponent
   */
  @Output() commit = new EventEmitter();

  /**
   * Fired when a requested action has been cancelled.
   *
   * @memberof IdentityEditComponent
   */
  @Output() cancel = new EventEmitter<KeyInfo>();

  /**
   * Gets or sets readonly flag for component
   *
   *
   * @memberof KeyEditFormComponent
   */
  @Input() readOnly = false;

  /**
   * Gets or sets valid setting for component
   *
   * @memberof KeyEditFormComponent
   */
  @Output() isValid = true;

  /**
     * Initializes a new instance of this component.
     *
     * @param {Store<AppState>} store - the app store to inject.
     * @param {FormBuilder} formBuilder - the form builder instance to use for building HTML form.
  */
  constructor(
    store: Store<AppState>,
    formBuilder: FormBuilder
  ) {
      super(store, formBuilder);
      this.populateValidationMessages();
  }

  /**
  * Builds an HTML form for editing {KeyInfo} data.
  */
  buildForm(callback: (isCreate: boolean) => void) {
    callback(this.data.Id <= 0);
    return this.formBuilder.group({
      Key: [this.data.Key, FormValidators.forGuid()]
    });
  }

   /**
   * Populates error messages for HTML form validation.
   *
   * @private
   *
   * @memberof KeyEditFormComponent
   */
  private populateValidationMessages() {
    this.errors.Key = '';
    this.messages.Key = {
      required: ValidationMessages.keyRequired,
      pattern: ValidationMessages.keyInvalid
    };
  }

  /**
   * Change entity data on change of key value at form (if form field was validated)
   *
   *
   * @memberof KeyEditFormComponent
   */
  keyInfoChanged() {
    this.isValid = !this.errors.Key;
    if (this.isValid) {
      this.data.Key = this.form.value.Key;
    }
  }
}
