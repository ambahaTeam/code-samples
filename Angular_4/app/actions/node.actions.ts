import { NodeDefinition,
         EntityType,
         NodeAddress,
         Issuer,
         NodeSettings,
         EndpointSummary,
         MonitoringActivitiesRequest,
         MonitoringActivity
} from '../prodapp-core/model';
import * as domain from './domain.actions';
import { label } from '../prodapp-core/utils/Actions';
import { Action } from '@ngrx/store';

/**
 * Action Dictionary -- Typed labels for actions for use in reducers
 */
export class ActionTypes {
  static readonly LOAD_NODE_ERROR = label('[ Node ] Node Load Error');
  static readonly UPDATE_NODE = label('[ Node ] Update Node');
  static readonly DELETE_NODE_INSTANCE = label('[ Node ] Delete Node Instance');
  static readonly UPDATE_NODE_BASE_ADDRESS = label('[ Node ] Update Node Base Address');
  static readonly DELETE_NODE_BASE_ADDRESS = label('[ Node ] Delete Node Base Address');
  static readonly UPDATE_NODE_TRUSTED_NAMED_ISSUER = label('[ Node ] Update Node Trusted Named Issuer');
  static readonly DELETE_NODE_TRUSTED_NAMED_ISSUER = label('[ Node ] Delete Node Trusted Named Issuer');
  static readonly UPDATE_NODE_SETTINGS = label('[ Node ] Update Node Advanced Data');
  static readonly GET_NODE_ENDPOINTS = label('[ Node ] Get Node Endpoints');
  static readonly GET_NODE_ENDPOINTS_SUCCESS = label('[ Node ] Get Node Endpoints Success');
  static readonly GET_NODE_ACTIVITY_LOGS = label('[ Node ] Get Node Activity Logs');
  static readonly GET_NODE_ACTIVITY_LOGS_SUCCESS = label('[ Node ] Get Node Activity Logs Success');
};

/**
 * Load Node Action
 *
 * @export
 * @class LoadNodeAction
 * @extends {domain.LoadDomainEntityAction<Node>}
 */
export class LoadNodeAction extends domain.LoadDomainEntityAction<NodeDefinition> {
  /**
   * Creates an instance of LoadNodeAction.
   *
   * @param {number} id
   *
   * @memberof LoadNodeAction
   */
  constructor(id: number) {
    super({id: id, domain: EntityType.Node});
  }
}

/**
 * Load Node Success Action
 *
 * @export
 * @class LoadNodeSuccessAction
 * @extends {domain.LoadDomainEntitySuccess<Node>}
 */
export class LoadNodeSuccessAction extends domain.LoadDomainEntitySuccess<NodeDefinition> {
  /**
   * Creates an instance of LoadNodeSuccessAction.
   *
   * @param {NodeDefinition} node
   *
   * @memberof LoadNodeSuccessAction
   */
  constructor(node: NodeDefinition) {
    super({ entity: node, domain: EntityType.Node });
  }
}

/**
 * Update a Node
 *
 * @export
 * @class UpdateNodeAction
 * @extends {domain.UpdateDomainObject<Node>}
 */
export class UpdateNodeAction extends domain.UpdateDomainEntity<NodeDefinition> {

  /**
   * Creates an instance of UpdateNodeAction.
   *
   * @param {NodeDefinition} node - the node definition to create or update.
   * @param {boolean} withResult - true to return the updated node definition, false to return just id.
   *
   * @memberof UpdateNodeAction
   */
  constructor(node: NodeDefinition, withResult?: boolean) {
    super({ entity: node, domain: EntityType.Node, withResult});
  }
}

/**
 * Delete node instance action
 *
 * @export
 * @class DeleteNodeInstanceAction
 * @implements {Action}
 */
export class DeleteNodeInstanceAction implements Action {

  /**
   * Defines the action type.
   *
   *
   * @memberof DeleteNodeInstanceAction
   */
  public readonly type = ActionTypes.DELETE_NODE_INSTANCE;

  /**
   * Creates an instance of DeleteNodeInstanceAction.
   * @param {{ node: NodeDefinition, id: number}} payload - node definition and deleted node instance id
   *
   * @memberof DeleteNodeInstanceAction
   */
  constructor(public payload: { node: NodeDefinition, id: number}) {}
}

/**
 * Update node address action
 *
 * @export
 * @class UpdateNodeBaseAddressAction
 * @implements {Action}
 */
export class UpdateNodeBaseAddressAction implements Action {

  /**
   * Defines the action type.
   *
   * @memberof UpdateNodeBaseAddressAction
   */
  public readonly type = ActionTypes.UPDATE_NODE_BASE_ADDRESS;

  /**
   * Creates an instance of UpdateNodeBaseAddressAction.
   * @param {{ node: NodeDefinition, address: NodeAddress}} payload - node definition and address
   *
   * @memberof UpdateNodeBaseAddressAction
   */
  constructor(public payload: { node: NodeDefinition, address: NodeAddress}) {}
}

/**
 * Delete node base address action
 *
 * @export
 * @class DeleteNodeBaseAddressAction
 * @implements {Action}
 */
export class DeleteNodeBaseAddressAction implements Action {

  /**
   * Defines the action type.
   *
   *
   * @memberof DeleteNodeBaseAddressAction
   */
  public readonly type = ActionTypes.DELETE_NODE_BASE_ADDRESS;

  /**
   * Creates an instance of DeleteNodeBaseAddressAction.
   * @param {{ node: NodeDefinition, id: number}} payload - node definition and id of deleted address
   *
   * @memberof DeleteNodeBaseAddressAction
   */
  constructor(public payload: { node: NodeDefinition, id: number}) {}
}

/**
 * Update trusted named issuer action
 *
 * @export
 * @class UpdateTrustedNamedIssuerAction
 * @implements {Action}
 */
export class UpdateTrustedNamedIssuerAction implements Action {

  /**
   * Defines the action type.
   *
   *
   * @memberof UpdateTrustedNamedIssuerAction
   */
  public readonly type = ActionTypes.UPDATE_NODE_TRUSTED_NAMED_ISSUER;

  /**
   * Creates an instance of UpdateTrustedNamedIssuerAction.
   * @param {{ node: NodeDefinition, issuer: Issuer}} payload - node definition and issuer
   *
   * @memberof UpdateTrustedNamedIssuerAction
   */
  constructor(public payload: { node: NodeDefinition, issuer: Issuer}) {}
}

/**
 * Delete trusted named issuer action
 *
 * @export
 * @class DeleteTrustedNamedIssuerAction
 * @implements {Action}
 */
export class DeleteTrustedNamedIssuerAction implements Action {

  /**
   * Defines the action type.
   *
   * @memberof DeleteTrustedNamedIssuerAction
   */
  public readonly type = ActionTypes.DELETE_NODE_TRUSTED_NAMED_ISSUER;

  /**
   * Creates an instance of DeleteTrustedNamedIssuerAction.
   * @param {{ node: NodeDefinition, id: number}} payload - node definition and id of deleted issuer
   *
   * @memberof DeleteTrustedNamedIssuerAction
   */
  constructor(public payload: { node: NodeDefinition, id: number}) {}
}

/**
 * Update node settings action
 *
 * @export
 * @class UpdateNodeSettingsAction
 * @implements {Action}
 */
export class UpdateNodeSettingsAction implements Action {

  /**
   * Defines the action type.
   *
   *
   * @memberof UpdateNodeSettingsAction
   */
  public readonly type = ActionTypes.UPDATE_NODE_SETTINGS;

  /**
   * Creates an instance of UpdateNodeSettingsAction.
   * @param {{ node: NodeDefinition, settings: NodeSettings}} payload - node definition and settings
   *
   * @memberof UpdateNodeSettingsAction
   */
  constructor(public payload: { node: NodeDefinition, settings: NodeSettings}) {}
}

/**
 * Get node endpoints action
 *
 * @export
 * @class GetNodeEndpointsAction
 * @implements {Action}
 */
export class GetNodeEndpointsAction implements Action {

  /**
   * Defines the action type
   * @memberof GetNodeEndpointsAction
   */
  public readonly type = ActionTypes.GET_NODE_ENDPOINTS;

  /**
   * Creates an instance of GetNodeEndpointsAction.
   * @param {{ nodeId: number}} payload - node id
   *
   * @memberof GetNodeEndpointsAction
   */
  constructor(public payload: { nodeId: number}) {}
}

/**
 * Get node endpoints success action
 *
 * @export
 * @class GetNodeEndpointsSuccessAction
 * @implements {Action}
 */
export class GetNodeEndpointsSuccessAction implements Action {

  /**
   * Defines action type
   * @memberof GetNodeEndpointsSuccessAction
   */
  public readonly type = ActionTypes.GET_NODE_ENDPOINTS_SUCCESS;

  /**
   * Creates an instance of GetNodeEndpointsSuccessAction.
   * @param {{ endpoints: EndpointSummary[]}} payload - array with endpoints
   *
   * @memberof GetNodeEndpointsSuccessAction
   */
  constructor(public payload: { endpoints: EndpointSummary[]}) {}
}

/**
 * Get node activities action
 * @export
 * @class GetNodeActivitiesAction
 * @implements {Action}
 */
export class GetNodeActivitiesAction implements Action {
  /**
   * Defines action type
   * @memberof GetNodeActivitiesAction
   */
  public readonly type = ActionTypes.GET_NODE_ACTIVITY_LOGS;
  /**
   * Creates an instance of GetNodeActivitiesAction.
   * @param {{ request: MonitoringActivitiesRequest}} payload - activities request
   *
   * @memberof GetNodeActivitiesAction
   */
  constructor(public payload: { request: MonitoringActivitiesRequest}) {}
}

/**
 * Get node activities success action
 * @export
 * @class GetNodeActivitiesSuccessAction
 * @implements {Action}
 */
export class GetNodeActivitiesSuccessAction implements Action {
  /**
   * Defines action type
   * @memberof GetNodeActivitiesSuccessAction
   */
  public readonly type = ActionTypes.GET_NODE_ACTIVITY_LOGS_SUCCESS;

  /**
   * Creates an instance of GetNodeActivitiesSuccessAction.
   * @param {{ request: MonitoringActivitiesRequest, activities: MonitoringActivity[]}} payload - monitoring request and activities
   *
   * @memberof GetNodeActivitiesSuccessAction
   */
  constructor(public payload: { request: MonitoringActivitiesRequest, activities: MonitoringActivity[]}) {}
}

/**
 * @type {
 *   LoadNodeAction
 * | LoadNodeSuccessAction
 * | LoadNodeErrorAction
 * | UpdateNodeAction
 * | DeleteNodeInstanceAction
 * | UpdateNodeBaseAddressAction
 * | DeleteNodeBaseAddressAction
 * | UpdateTrustedNamedIssuerAction
 * | DeleteTrustedNamedIssuerAction
 * | UpdateNodeSettingsAction
 * | GetNodeEndpointsAction
 * | GetNodeEndpointsSuccessAction
 * | GetNodeActivitiesAction
 * | GetNodeActivitiesSuccessAction }
 */
export type Actions =
    LoadNodeAction
  | LoadNodeSuccessAction
  | UpdateNodeAction
  | DeleteNodeInstanceAction
  | UpdateNodeBaseAddressAction
  | DeleteNodeBaseAddressAction
  | UpdateTrustedNamedIssuerAction
  | DeleteTrustedNamedIssuerAction
  | UpdateNodeSettingsAction
  | GetNodeEndpointsAction
  | GetNodeEndpointsSuccessAction
  | GetNodeActivitiesAction
  | GetNodeActivitiesSuccessAction;
