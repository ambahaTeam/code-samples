import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeAdvancedComponent } from './node-advanced.component';

describe('NodeAdvancedComponent', () => {
  let component: NodeAdvancedComponent;
  let fixture: ComponentFixture<NodeAdvancedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeAdvancedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeAdvancedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
