import { ConfigurationChangeMode } from '../enum/ConfigurationChangeMode';
import { ClientAddressOptions } from '../enum/ClientAddressOptions';
import { ISchemaTypeObject } from '../../interfaces/ISchemaTypeObject';

/**
 * Defines a set of advanced configuration properties for a node.
 *
 * @export
 * @class NodeSettings
 * @implements {ISchemaTypeObject}
 */
export class NodeSettings implements ISchemaTypeObject {

  __type: string;

  /**
   * Node configuration change mode
   *
   * @type {ConfigurationChangeMode}
   * @memberof NodeSettings
   */
  ChangeMode: ConfigurationChangeMode = ConfigurationChangeMode.ServiceRestart;

  /**
   * Gets or sets the name of the HTTP header providing the ultimate service caller IP address.
   *
   * @type {string}
   * @memberof NodeSettings
   */
  ClientAddressHeaderName: string = null;

  /**
   *  Defines a set of advanced configuration properties for a node.
   *
   * @type {ClientAddressOptions}
   * @memberof NodeSettings
   */
  ClientAddressOptions: ClientAddressOptions;

}
