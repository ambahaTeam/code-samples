import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

import * as moment from 'moment';

import { HttpService } from './http.service';
import { EntityServiceBase } from './base-entity.service';
import { NodeTypeMap, ConfigurationChangeModeMap } from '../../prodapp-core/constants';

import {
  GetNode,
  CreateOrUpdateNode,
  CreateOrUpdateNodeWithResult,
  DeleteNode,
  GetNodeEndpoints,
  GetActivities
} from '../../prodapp-core/api';

import {
  NodeDefinition,
  MonitoringActivity,
  EntityType,
  FolderEntityTypes,
  NodeAddressSetUI,
  NodeAddressKind,
  NodeAddress,
  Issuer,
  NodeSettings,
  EndpointSummary,
  MonitoringActivitiesRequest,
  NodeType
} from '../../prodapp-core/model';
import { NEW_ID } from '../../prodapp-core/constants';

import { uuid4, toFolderEntityName, ArrayHelper } from '../../prodapp-core/utils';

/**
 * An entity service that provides access to {Node} data.
 *
 * @export
 * @class NodesService
 * @extends {EntityServiceBase<number, NodeDefinition>}
 */
@Injectable()
export class NodesService extends EntityServiceBase<number, NodeDefinition> {

  /**
   * Creates an instance of NodesService.
   *
   * @param {HttpService} http - the injected {HttpService} that will be used to make REST API calls.
   *
   * @memberof NodesService
   */
  constructor(http: HttpService) {
    super(http);
  }

  /**
   * @inheritdoc
   */
  newInstance(folderId: number = null, entityType = FolderEntityTypes.Node): NodeDefinition {
      const node = new NodeDefinition();
      node.Id = NEW_ID;
      node.IsEnabled = true;
      node.NodeType = NodeType.IisMixedTransportsMode;
      node.LastUpdate = moment.min();
      node.Name = `New ${toFolderEntityName(FolderEntityTypes.Node)}`;
      node.Key = uuid4();
      node.FolderId = folderId > 0 ? folderId : null;
      node.ObjectType = EntityType.Node;
      return node;
  }

  /**
   * @inheritdoc
   */
  get(key: number): Observable<NodeDefinition> {
    return this.http
      .get(`${GetNode}?nodeId=${key}`)
      .map(res => res.json());
  }

  /**
   * @inheritdoc
   */
  protected createOrUpdateCore(node: NodeDefinition, withResult?: boolean, xml?: string): Observable<Response> {

    const uri = withResult ?
      `${CreateOrUpdateNodeWithResult}` :
      `${CreateOrUpdateNode}`;

    return this.http
      .post(uri, node);
  }

  /**
   * @inheritdoc
   */
  delete(key: number) {
    return this.http
      .delete(`${DeleteNode}?nodeId=${key}`)
      .map(() => key);
  }

  /**
   * Gets the node kind's friendly name: either("ASP.NET Compatibility" or "Mixed Transports").
   *
   * @param {number} kind - the node kind.
   * @returns {string} - the translated node kind name.
   *
   * @memberof NodesService
   */
  getKindText(kind: number): string {
    return NodeTypeMap[kind];
  }

   /**
    * Gets the mapping of node configuration change modes
    *
    * @returns {NodeConfigurationChangeModeMap}
    *
    * @memberof NodesService
    */
   getDisplayNamesForNodeConfigurationChangeModes() {
     return ConfigurationChangeModeMap;
   }

  /**
   * Get array with base address sets filtered by address kind
   *
   * @param {NodeAddress[]} addresses - input addresses
   * @returns - filtered sets
   *
   * @memberof NodesService
   */
  getBaseAddressSets(addresses: NodeAddress[]): NodeAddressSetUI[] {
    const result = new Array<NodeAddressSetUI>();
    for (const n in NodeAddressKind) {
      if (typeof NodeAddressKind[n] === 'number') {
          const set = new NodeAddressSetUI(addresses.filter((a) => {
            return a.Kind.toString() === NodeAddressKind[n].toString();
          }), +NodeAddressKind[n] as NodeAddressKind);
          result.push(set);
      }
    }
    return result;
  }
  
  ...

  /**
   * Get node endpoints
   * @param {number} nodeId - node id
   * @returns {Observable<EndpointSummary[]>} - observable with node endpoints
   *
   * @memberof NodesService
   */
  getNodeEndpoints(nodeId: number): Observable<EndpointSummary[]> {
    return this.http
      .get(`${GetNodeEndpoints}?nodeId=${nodeId}`)
      .map(res => res.json());
  }

  /**
   * Gets node activities
   * @param {MonitoringActivitiesRequest} request - monitoring activities request
   * @returns {Observable<MonitoringActivity[]>} - array with node monitoring activity records.
   *
   * @memberof NodesService
   */
  getNodeActivities(request: MonitoringActivitiesRequest): Observable<MonitoringActivity[]> {
    return this.http
      .post(`${GetActivities}`, request)
      .map(res => res.json());
  }
}
