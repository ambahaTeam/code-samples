import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NodeInstancesGridComponent } from './node-instances-grid.component';


describe('NodeInstancesGridComponent', () => {
  let component: NodeInstancesGridComponent;
  let fixture: ComponentFixture<NodeInstancesGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeInstancesGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeInstancesGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
