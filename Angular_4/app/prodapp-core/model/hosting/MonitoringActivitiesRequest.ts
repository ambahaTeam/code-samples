import { Severity, SortOrder, ActivitySortField } from '../enum';
import { DateRangeSearchRequest } from '../search';
import { ISchemaTypeObject } from '../../interfaces/ISchemaTypeObject';

import * as moment from 'moment';


/**
 * Defines parameters for get activities method.
 * @export
 * @class MonitoringActivitiesRequest
 * @extends {DateRangeSearchRequest}
 */
export class MonitoringActivitiesRequest extends DateRangeSearchRequest implements ISchemaTypeObject {

    __type: string;

    /**
    /*     Gets or sets the transaction identifier associated with an activity record to be returned.
    /*     Note that if Transaction Id is provided then all other properties are ignored and search
    /*     is performed by this transaction alone.
    **/
    TransactionId?: string;

    /**
    /*     Gets or sets the highest severity level of an activity record to be returned.
    /*     Default is <see cref="Severity.Error"/>.
    **/
    MinimumSeverity: Severity = Severity.T;

    /**
    /*     Gets or sets the identifier of the node that reported activity records. If not set then
    /*     records from all nodes are returned.
    **/
    NodeId?: number;

    /**
    /*     Gets or sets the identifier of the node instance that reported activity records. If not
    /*     set then records from all node instances are returned.
    **/
    NodeInstanceId?: number;

    /**
    /*     Gets or sets the identifier of the service version that activity records are bound to.
    /*     If not set then records are not limited to a specific service version.
    **/
    ServiceVersionId?: number;

    /**
    /*     Gets or sets the field that result will be sorted by.
    **/
    SortField = ActivitySortField.ActivityDateTime;

    /**
    /*     Gets or sets the result sort order.
    **/
    SortOrder: SortOrder = SortOrder.Descending;

    /**
    /*     Gets or sets the string to be found in the activity message (wildcards are allowed). If not
    /*     set then activities are not filtered by the message.
    **/
    ActivityMessage: string;

    /**
    /*     Gets or sets the flag indicating that activities associated with monitoring transactions
    /*     should NOT be included in the result. This flag cannot be set, if <see cref="TransactionId"/>
    /*     property is populated.
    **/
    IgnoreTransactionActivities: boolean;

    /**
     * Creates an instance of MonitoringActivitiesRequest.
     * @param {moment.Moment} [startDateTime=moment().subtract(1, 'hours')] - start date
     * @param {moment.Moment} [endDateTime] - end date
     *
     * @memberof MonitoringActivitiesRequest
     */
    constructor(
        startDateTime: moment.Moment = moment().subtract(1, 'hours'),
        endDateTime?: moment.Moment) {
        super(startDateTime, endDateTime);
    }
}
