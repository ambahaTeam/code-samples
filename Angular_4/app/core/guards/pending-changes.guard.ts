import { Injectable, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AppState, canNavigate, getNavigationDisabledReason } from '../../reducers';
import { EnableNavigationAction, DisableNavigationAction } from '../../actions/app.actions';
import { CancelSelectNodeAction } from '../../actions/repository.actions';
import { DialogService } from '../../shared/dialog';
import { ID_PARAM, NEW_PARAM_VALUE } from '../..';


/**
 * When realized by a component, will allow a callback to be executed
 * when navigation occurs away from a component.
 *
 * @export
 * @interface OnPendingChangesNavigateComponent
 * @extends {Component}
 */
export interface OnPendingChangesNavigate extends Component {
  onPendingChangesNavigate(): void;
}

/**
* A route deactivation guard that checks whether there are pending, uncommitted
* changes and prompts user to confirm.
*/
@Injectable()
export class PendingChangesGuard implements CanDeactivate<any> {

  private readonly _canNavigate$ = this.store.select(canNavigate);
  private readonly _reason$ = this.store.select(getNavigationDisabledReason);

  /**
    * Initializes a new instance of {PendingChangesGuard} guard.
    *
    * @param {Store<AppState>} store - the app store for storing the loaded entity.
    * @param {Router} router - the Angular router for navigation redirects.
    */
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private dialog: DialogService
  ) {
  }


  /**
   *  Prevents navigation away from a page that has form with pending changes.
   *
   * @param {OnPendingChangesNavigateComponent} component - the current component that is being deactivated.
   * @param {ActivatedRouteSnapshot} route - the current route snapshot.
   * @param {RouterStateSnapshot} state - the router state.
   * @returns {Observable<boolean>} - the observable that Angular's router will subscribe to awaiting the user input
   * on whether the navigation away from this route is allowed or not.
   *
   * @memberof NavigationGuard
   */
  canDeactivate(
    component: OnPendingChangesNavigate,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {

    return this._canNavigate$
      .combineLatest(this._reason$)
      .mergeMap(([canNavigate, reason]) => {
        if (!canNavigate) {
          return new Observable<boolean>((observer) => {
            this.dialog.confirm('Confirm Navigation', reason)
              .afterClosed().subscribe(confirmed => {
                if (confirmed) {
                  // Fix for bug #2986
                  // Created node must be deleted only if user confirmed redirect from created node itself, which has negative id.
                  // If user navigates away from any other node - created node stays in repository tree.

                  // Get id from parameters
                  const id = route.params[ID_PARAM];

                  // If id has negative value - it means user confirmed redirect from created node
                  const deleteCreatedNode = id && (+id < 0 || id === NEW_PARAM_VALUE);

                  // Dispatch enable navigation action
                  this.store.dispatch(new EnableNavigationAction({
                    clearPendingChanges: true,
                    deleteCreatedNode
                  }));

                  if (component.onPendingChangesNavigate) {
                    component.onPendingChangesNavigate();
                  }
                } else {
                  this.store.dispatch(new CancelSelectNodeAction());
                  this.router.navigateByUrl(state.url);
                  // Enable navigation but do not clear pending changes flag - this is needed to trigger state change
                  this.store.dispatch(new EnableNavigationAction());
                  // Disable navigation back with the same reason
                  this.store.dispatch(new DisableNavigationAction(reason));
                }
              });
          });
        }
        return Observable.of(canNavigate);
      });
  }
}
