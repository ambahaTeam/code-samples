import { Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { go } from '@ngrx/router-store';

import { IEntityService } from '../core/services';
import {
  DomainObjectBase,
  EntityType
} from '../prodapp-core/model';
import { DomainObjectHelper } from '../prodapp-core/utils';
import { RepositoryNode } from '../prodapp-core/repository';
import * as domainActions from '../actions/domain.actions';
import * as errorsActions from '../actions/error.actions';
import * as processingActions from '../actions/processing.actions';
import * as repositoryActions from '../actions/repository.actions';
import { RoutingTable } from '../app.routing.table';
import { NEW_ID } from '../prodapp-core/constants';
import { ICloneableEntityService } from '../core/services/base-entity.service';

export interface IEntityEffects<TKey extends string | number, TEntity extends DomainObjectBase>{
}

export abstract class EntityEffectsBase<TKey extends string | number, TEntity extends DomainObjectBase>
  implements IEntityEffects<TKey, TEntity> {

  protected constructor(
    protected readonly actions$: Actions,
    protected readonly service: IEntityService<TKey, TEntity>,
    protected entityType: EntityType
  ) { }

  /**
   * Stream for entity update action with returning of entity id as a result
   *
   * @protected
   * @param {(entity: TEntity) => Action[]} [onSuccess] - the optional callback that creates store actions to be
   * dispatched after entity has been successfully updated.
   * @returns
   *
   * @memberof EntityEffectsBase
   */
  protected createEntityUpdateStream(onSuccess?: (entity: TEntity) => Action[]) {
    return this.actions$
    .ofType(domainActions.ActionTypes.UPDATE_DOMAIN_ENTITY)
    .map((action: domainActions.UpdateDomainEntity<TEntity>) => action.payload)
    .filter(({domain}) => domain === this.entityType)
    .switchMap(({entity, withResult}) => {

      const isCreate = entity.Id === NEW_ID;

      return this.includeProcessing(
        this.service
          .createOrUpdate(entity, withResult)
          .mergeMap(e => {
              return this.processActions(onSuccess, e, isCreate);
          })
          .catch(this.handleError)
      );
    });
  }

  protected createLoadEntityStream(onSuccess?: (entity: TEntity, id: TKey) => Action[]) {
    return this.actions$
      .ofType(domainActions.ActionTypes.LOAD_DOMAIN_ENTITY)
      .map((action: domainActions.LoadDomainEntityAction<TEntity>) => action.payload)
      .filter(({domain}) => domain === this.entityType)
      .concatMap(({id}) => this.includeProcessing(
        this.service
          // TODO: Solve this typing issue...
          .get(<TKey>id)
          .mergeMap(entity => {
            const actions = onSuccess ? onSuccess(entity, <TKey>id) ||  [] : [];
            actions.push(new domainActions.LoadDomainEntitySuccess({ entity, domain: this.entityType }));
            return actions;
          })
          .catch(this.handleError)
      ));
  }


  protected handleError(error: Error) {
    return Observable.of(new errorsActions.ErrorAction({ error }));
  }

  /**
   * Stream for update action of entity key info with dispatching of load action for entity as a success
   *
   * @protected
   * @returns
   * @memberof EntityEffectsBase
   */
  protected createEntityUpdateKeyInfoStream() {
    return this.actions$
    .ofType(domainActions.ActionTypes.UPDATE_ENTITY_KEY_INFO)
    .map((action: domainActions.UpdateEntityKeyInfoAction<TEntity>) => action.payload)
    .filter(({domain}) => domain === this.entityType)
    .switchMap(({entity, keyInfo}) => {
      return this.service.updateKeyInfo(entity, keyInfo)
        .map(e => {
            return new domainActions.LoadDomainEntitySuccess({ entity: e, domain: this.entityType });
        })
        .catch(this.handleError)
    });
  }

  /**
   * Include processing during async operations
   *
   * @protected
   * @param {...Observable<any>[]} methods
   * @returns {Observable<Action>}
   *
   * @memberof EffectsBase
   */
  protected includeProcessing(...methods: Observable<any>[]): Observable<Action> {
      return Observable.concat(
          Observable.of(new processingActions.ShowProcessingAction()),
          ...methods,
          Observable.of(new processingActions.HideProcessingAction())
      );
  }

  /**
   * Common method for update streams which prepares list of necessary actions
   *
   * @private
   * @param {(entity: TEntity, isVirtual: boolean) => Action[]} onSuccess - success callback
   * @param {TEntity} entity - updated entity
   * @param {boolean} isCreate - indicated wether this is a create or update operation.
   * @param {boolean} isVirtual - indicates whether node is virtual.
   * @returns {Action[]} - array of actions
   *
   * @memberof EntityEffectsBase
   */
  protected processActions(onSuccess: (entity: TEntity, isVirtual: boolean) => Action[],
      entity: TEntity,
      isCreate: boolean, isVirtual = false) {

      const actions = onSuccess ? onSuccess(entity, isVirtual) ||  [] : [];

      if (isCreate) {
        // Navigate to the path with assigned id after new entity is saved
        const folderEntityType = DomainObjectHelper.getFolderEntityType(entity, this.entityType);
        const route = RoutingTable.forEntityType(folderEntityType);
        if (route) {
          actions.push(go([route.path, entity.Id]));
          actions.push(new repositoryActions.SelectNodeAction(new RepositoryNode(entity.Id, folderEntityType)));
        }
      }

      return actions;
  }
}

/**
 * A base class for cloneable entities effects.
 *
 * @export
 * @abstract
 * @class EntityCloneableEffectsBase
 * @extends {EntityEffectsBase<TKey, TEntity>}
 */
export abstract class EntityCloneableEffectsBase<TKey extends string | number, TEntity extends DomainObjectBase>
  extends EntityEffectsBase<TKey, TEntity> {

  /**
   * Creates an instance of EntityCloneableEffectsBase.
   * @param {Actions} actions$ - the observable stream of dispatched store actions.
   * @param {ICloneableEntityService<TKey, TEntity>} service - the injected entity service.
   * @param {EntityType} entityType - entity type.
   *
   * @memberof EntityCloneableEffectsBase
   */
  protected constructor(
    actions$: Actions,
    protected readonly service: ICloneableEntityService<TKey, TEntity>,
    entityType: EntityType
  ) {
    super(actions$, service, entityType);
  }

  /**
   * Stream for entity clone action with returning of entity as a result
   *
   * @protected
   * @param {(entity: TEntity, isVirtual: boolean) => Action[]} [onSuccess] - callback that creates store actions to be
   * dispatched after entity has been successfully cloned.
   * @param {(node: RepositoryNode) => boolean} [filterFunction] - callback that filters unnecessary entities out.
   * @returns
   *
   * @memberof EntityCloneableEffectsBase
   */
  protected createEntityCloneStream(onSuccess: (entity: TEntity, isVirtual: boolean) => Action[],
                                    filterFunction: (node: RepositoryNode) => boolean) {
    return this.actions$
      .ofType(repositoryActions.ActionTypes.CLONE_NODE)
      .map((action: repositoryActions.CloneNodeAction) => action.payload)
      .filter(node => filterFunction(node))
      .switchMap(payload => {
        return this.includeProcessing(
          this.service.clone(payload.Id)
            .mergeMap(e => {
              return this.processActions(onSuccess, <TEntity>e, true, payload.isVirtual);
            })
            .catch(this.handleError)
        )
      });
  }

}
