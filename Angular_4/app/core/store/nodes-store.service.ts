import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { createSelector } from 'reselect';
import { AppState } from '../../reducers';
import { State } from '../../reducers/node.reducer';
import { getSelectedNode } from '../../reducers/selected.reducer';
import * as nodeActions from '../../actions/node.actions';
import { NodeDefinition,
  EntityType,
  NodeAddress,
  Issuer,
  NodeSettings,
  EndpointSummary,
  MonitoringActivity,
  MonitoringActivitiesRequest,
  AzureIdentityDefinition,
  IdentityKind,
  AzureCredentialType,
  AzureNetworkCredential,
  NodeAddressKind,
  AzureSecurityConfiguration,
  IAzureIdentityData,
  NodeType,
  EndpointDefinition,
  ContractDefinition,
} from '../../prodapp-core/model';
import { StoreServiceBase } from './base-store.service';
import { DialogService } from '../../shared/dialog';
import { NEW_ID, DEFAULT_SERVICE_EXTENSION } from '../../prodapp-core/constants';
import { uuid4, IDictionary, StringHelper } from '../../prodapp-core/utils';

/**
 * The store service implementation that encapsulates access to the node state.
 *
 * @export
 * @class NodesStoreService
 * @extends {StoreServiceBase<State>}
 */
@Injectable()
export class NodesStoreService extends StoreServiceBase<State> {

  private _newAddressId = NEW_ID;

  protected readonly state = (state: AppState) => state.nodes;
  private readonly _entities = (state: State) => state.entities;

  private readonly _endpoints = (state: State) => state.endpoints;
  private readonly _activities = (state: State) => state.activities;
  private readonly _monitoringActivityRequest = (state: State) =>
    Object.assign({}, state.monitoringActivityRequest);

  // Selectors
  private readonly _entitiesSelector = () => createSelector(this.state, this._entities);
  private readonly _currentNodeIdSelector = () => createSelector(this.selectedState, getSelectedNode);
  private readonly _endpointsSelector = () => createSelector(this.state, this._endpoints);
  private readonly _activitiesSelector = () => createSelector(this.state, this._activities);
  private readonly _monitoringActivityRequestSelector = () => createSelector(this.state, this._monitoringActivityRequest);

  /**
   * Creates an instance of BindingsStoreService.
   * @param {Store<AppState>} store - the injected app store.
   * @param {DialogService} dialog - the injected dialog service.
   *
   * @memberof NodesStoreService
   */
  constructor(store: Store<AppState>, dialog: DialogService) {
    super(store, dialog);
  }

  /**
   * Gets the single node by id from the state.
   *
   * @param {number} id = the node id.
   * @returns {Observable<Binding>} - the observable that provides access to the node definition with the specified id.
   *
   * @memberof NodesStoreService
   */
  getOne(id: number): Observable<NodeDefinition> {
    return this.store.select(this.getDomainObject<NodeDefinition>(id, EntityType.Node));
  }

  /**
   * Gets all nodes that are currently in the state.
   *
   * @returns {Observable<IDictionary<number, Node>>} - the observable of dictionary where
   * key is the node id and value is {Node} instance.
   *
   * @memberof NodesStoreService
   */
  getAll(): Observable<IDictionary<number, NodeDefinition>> {
    return this.store.select(this._entitiesSelector());
  }

  /**
   * Gets the currently selected node id from the state.
   *
   * @returns {Observable<number>} - the observable of selected node id.
   *
   * @memberof NodesStoreService
   */
  getSelectedId(): Observable<number> {
    return this.store.select(this._currentNodeIdSelector());
  }

  /**
   * Gets the currently selected node from the state.
   *
   * @returns {Observable<Node>} - the observable of selected node.
   *
   * @memberof NodesStoreService
   */
  getSelected(): Observable<NodeDefinition> {
    return this.store.select(
        createSelector(
          this._entitiesSelector(),
          this._currentNodeIdSelector(),
          (entities, id) => entities.getValue(id)));
  }

  /**
   * Saves the node.
   *
   * @param {Node} node - the node to save.
   * @param {boolean} withResult - the optional flag that indicates whether an updated instance
   * should be returned from the back-end or just id. The default value is false (only id is returned).
   * @memberof NodesStoreService
   */
  save(node: NodeDefinition, withResult?: boolean) {
    if (node) {
      this.store.dispatch(new nodeActions.UpdateNodeAction(node, withResult));
    }
  }

  /**
   * Refreshes the nodes data.
   *
   * @param {number} id - the id of node to refresh.
   *
   * @memberof NodesStoreService
   */
  refresh(id: number) {
    this.loadById(id);
    this.clearPendingChanges();
  }

  /**
   * Loads node definition by id
   *
   * @param {number} id - node id
   * @memberof NodesStoreService
   */
  loadById(id: number) {
    this.store.dispatch(new nodeActions.LoadNodeAction(id));
  }

  /**
   * Deletes node instance
   *
   * @param {NodeDefinition} node - node definition
   * @param {number} id - deleted node instance id
   *
   * @memberof NodesStoreService
   */
  deleteInstance(node: NodeDefinition, id: number) {
      if ( node && id ) {
        this.store.dispatch(
            new nodeActions.DeleteNodeInstanceAction({
                node: node,
                id: id
            }));
        this.setPendingChanges();
      }
  }

  /**
   * Updates node base address
   *
   * @param {NodeDefinition} node - node definition
   * @param {NodeAddress} address - updated node address
   *
   * @memberof NodesStoreService
   */
  updateBaseAddress(node: NodeDefinition, address: NodeAddress) {
      if ( node && address ) {
        this.store.dispatch(
            new nodeActions.UpdateNodeBaseAddressAction({
                node,
                address
            }));
        this.setPendingChanges();
      }
  }

  /**
   * Deletes node base address
   *
   * @param {NodeDefinition} node - node definition
   * @param {number} id - deleted node address id
   *
   * @memberof NodesStoreService
   */
  deleteBaseAddress(node: NodeDefinition, id: number) {
    if ( node && id ) {
      this.store.dispatch(
          new nodeActions.DeleteNodeBaseAddressAction({
              node: node,
              id: id
          }));
      this.setPendingChanges();
    }
  }

  /**
   * Updates node trusted named issuer
   *
   * @param {NodeDefinition} node - node definition
   * @param {Issuer} issuer - trusted named issuer
   *
   * @memberof NodesStoreService
   */
  updateIssuer(node: NodeDefinition, issuer: Issuer) {
    if ( node && issuer ) {
      this.store.dispatch(
          new nodeActions.UpdateTrustedNamedIssuerAction({
              node: node,
              issuer: issuer
          }));
      this.setPendingChanges();
    }
  }

  /**
   * Deletes trusted named issuer
   *
   * @param {NodeDefinition} node - node definition
   * @param {number} id - deleted trusted named issuer id
   *
   * @memberof NodesStoreService
   */
  deleteIssuer(node: NodeDefinition, id: number) {
    if ( node && id ) {
      this.store.dispatch(
          new nodeActions.DeleteTrustedNamedIssuerAction({
              node: node,
              id: id
          }));
      this.setPendingChanges();
    }
  }

  /**
   * Updates node settings
   *
   * @param {NodeDefinition} node - node definition
   * @param {NodeSettings} settings - updated settings
   *
   * @memberof NodesStoreService
   */
  updateSettings(node: NodeDefinition, settings: NodeSettings) {
    if ( node && settings ) {
      this.store.dispatch(
          new nodeActions.UpdateNodeSettingsAction({
              node: node,
              settings: settings
          }));
      this.setPendingChanges();
    }
  }

  /**
   * Get node endpoints by node id
   *
   * @param {number} id - node id
   *
   * @memberof NodesStoreService
   */
  getNodeEndpoints(id: number) {
    if (id) {
      this.store.dispatch(
          new nodeActions.GetNodeEndpointsAction({
            nodeId: id
          }));
    }
  }

  /**
   * Get currently loaded endpoints from store
   *
   * @returns {Observable<EndpointSummary[]>}
   *
   * @memberof NodesStoreService
   */
  getEndpoints(): Observable<EndpointSummary[]> {
    return this.store.select(this._endpointsSelector());
  }

  /**
   * Get node activities by request
   * @param {MonitoringActivitiesRequest} request - activities request
   *
   * @memberof NodesStoreService
   */
  getNodeActivities(request: MonitoringActivitiesRequest) {
    if (request) {
      this.store.dispatch(
        new nodeActions.GetNodeActivitiesAction({
          request
      }));
    }
  }

  /**
   * Get node activities from store
   *
   * @returns {Observable<MonitoringActivity[]>} - node activities
   *
   * @memberof NodesStoreService
   */
  getActivities(): Observable<MonitoringActivity[]> {
    return this.store.select(this._activitiesSelector());
  }

  /**
   * Get node activities request from store
   *
   * @returns {Observable<MonitoringActivitiesRequest>} - observable with request
   *
   * @memberof NodesStoreService
   */
  getActivitiesRequest(): Observable<MonitoringActivitiesRequest> {
    return this.store.select(this._monitoringActivityRequestSelector());
  }

  /**
   * Converts node base address to Azure identity definition
   *
   * @param {NodeAddress} address - node base address
   * @param {boolean} isModify - is entity modified flag
   * @returns {AzureIdentityDefinition}
   * @memberof NodesStoreService
   */
  convertBaseAddressToAzureIdentity(address: NodeAddress, isModify: boolean): AzureIdentityDefinition {
    const azureIdentity = new AzureIdentityDefinition();
    azureIdentity.Id = isModify ? address.Id : this._newAddressId--;
    azureIdentity.Key = isModify ? address.Key : uuid4();
    azureIdentity.Kind = IdentityKind.AzureCredentials;
    azureIdentity.Credentials.CredentialType = isModify ? address.Configuration.CredentialType : AzureCredentialType.None;
    azureIdentity.Credentials.StsUri = address.Configuration ? address.Configuration.StsUri : '';

    switch (azureIdentity.Credentials.CredentialType) {
      case AzureCredentialType.SharedSecret:
        azureIdentity.Credentials.SharedSecret = address.Configuration.SharedSecret;
      break;
      case AzureCredentialType.SharedAccessSignature:
        azureIdentity.Credentials.SharedAccessSignature = address.Configuration.SharedAccessSignature;
      break;
      case AzureCredentialType.OAuth:
        azureIdentity.Credentials.Credential = new AzureNetworkCredential();
        azureIdentity.Credentials.Credential.Domain = address.Configuration.Credential.Domain;
        azureIdentity.Credentials.Credential.UserName = address.Configuration.Credential.UserName;
        azureIdentity.Credentials.Credential.Password = address.Configuration.Credential.Password;
      break;
    }
    return azureIdentity;
  }

  /**
   * Converts Azure identity data into node address
   *
   * @param {IAzureIdentityData} azureIdentityData - Azure identity data
   * @param {number} nodeId - node id
   * @returns {NodeAddress}
   * @memberof NodesStoreService
   */
  convertAzureIdentityDataToNodeAddress(azureIdentityData: IAzureIdentityData, nodeId: number): NodeAddress {

    const credentials = azureIdentityData.identity.Credentials;
    const newAddress = new NodeAddress();
    newAddress.Address = azureIdentityData.address;
    newAddress.Id = azureIdentityData.identity.Id;
    newAddress.Key = azureIdentityData.identity.Key;
    newAddress.Kind = NodeAddressKind.AzureServiceBus;
    newAddress.NodeId = nodeId;

    newAddress.Configuration = new AzureSecurityConfiguration();
    newAddress.Configuration.StsUri = credentials.StsUri;
    newAddress.Configuration.CredentialType = credentials.CredentialType;

    switch (credentials.CredentialType) {
      case AzureCredentialType.SharedSecret:
        newAddress.Configuration.SharedSecret = credentials.SharedSecret;
      break;
      case AzureCredentialType.SharedAccessSignature:
        newAddress.Configuration.SharedAccessSignature = credentials.SharedAccessSignature;
      break;
      case AzureCredentialType.OAuth:
        newAddress.Configuration.Credential = new AzureNetworkCredential();
        newAddress.Configuration.Credential.Domain = credentials.Credential.Domain;
        newAddress.Configuration.Credential.UserName = credentials.Credential.UserName;
        newAddress.Configuration.Credential.Password = credentials.Credential.Password;
      break;
    }
    return newAddress;
  }

  /**
   * Returns formatted physical address for endpoint
   *
   * @param {NodeDefinition} node - node data
   * @param {NodeAddress} nodeAddress - node address
   * @param {string} baseAddress - base address for returned address
   * @returns {string}
   * @memberof NodesStoreService
   */
  getPhysicalAddressForEndpoint(node: NodeDefinition, nodeAddress: NodeAddress, baseAddress: string): string {
    // Append ".svc" to physical base addresses hosted on MixedTransports Node
    let baseAddressExtension = '';
    if (node.NodeType === NodeType.IisMixedTransportsMode && nodeAddress.Kind === NodeAddressKind.Physical
        && (!baseAddress.endsWith(DEFAULT_SERVICE_EXTENSION))) {
      baseAddressExtension = DEFAULT_SERVICE_EXTENSION;
    }

    // Add service base address only to physical addresses
    const serviceBaseAddress = nodeAddress.Kind === NodeAddressKind.Physical ? baseAddress : '';
    let effectiveBaseAddress = `${StringHelper.trim(nodeAddress.Address, '/')}/${serviceBaseAddress}${baseAddressExtension}`;
    effectiveBaseAddress = StringHelper.trim(effectiveBaseAddress, '/');

    return effectiveBaseAddress;
  }

  /**
   * Fills physical and logical addresses at provided endpoint
   *
   * @param {ContractDefinition} contract - contract data
   * @param {NodeDefinition} node - node data
   * @param {EndpointDefinition} endpoint - filled endpoint
   * @param {string} baseAddress - base address
   * @memberof NodesStoreService
   */
  fillEndpointAddresses(contract: ContractDefinition, node: NodeDefinition, endpoint: EndpointDefinition, baseAddress: string) {
    // Create address and check it for uniqueness
    let address = this.getPhysicalAddressForEndpoint(node, node.BaseAddresses[0], baseAddress);
    const addressExist = contract.Endpoints.findIndex(search => search.PhysicalAddress === address) >= 0;
    if (addressExist) {
      address += `/${uuid4()}`;
    }
    endpoint.PhysicalAddress = address;
    endpoint.LogicalAddress = address;
  }


  /**
   * Loads node from back-end and executes callback on finish
   *
   * @param {number} nodeId - loaded node id
   * @param {(node: NodeDefinition) => void} callback - callback function with loaded node data
   * @memberof NodesStoreService
   */
  loadNode(nodeId: number, callback: (node: NodeDefinition) => void) {
    this.getOne(nodeId)
      .filter(node => node && node.Id === nodeId).take(1).subscribe((node: NodeDefinition) => {
        callback(node);
    });
    this.loadById(nodeId);
  }
}
