﻿playerApp.service("playerService", function (networkService, crudService, $q) {
    var vm = this;

    vm.updateVideoViewDate = function (videoId) {
        return networkService.sendRequest(networkService.rbPost("POST").url("/User/Dashboard/UpdateViewDate").params({
            videoId: videoId
        }).build()).then(function (data) {
            return data;
        });
    };

    vm.getQuiz = function (quizId) {
        return networkService.sendRequest(networkService.rbPost("POST").url("/User/Dashboard/GetQuiz").params({
            quizId: quizId
        }).build()).then(function (data) {
            return data;
        });
    };

    vm.submitQuiz = function (quiz) {
        return networkService.sendRequest(networkService.rbPost("POST").url("/User/Dashboard/SubmitQuiz").body(JSON.stringify(quiz)).contentType('application/json').build()).then(function (data) {
            return data;
        });
    };

    return vm;

});