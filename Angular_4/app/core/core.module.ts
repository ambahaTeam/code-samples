import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { CoreServices } from './services';
import { StoreServices } from './store';
import { UserInfoComponent } from './user-info/user-info.component';
import { HeaderComponent } from './header/header.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NotAuthorizedComponent } from './not-authorized/not-authorized.component';
import { UserIsAdminGuard } from './guards/user-is-admin.guard';
import { PendingChangesGuard } from './guards/pending-changes.guard';
import { AggregateEntityService } from './services/aggregate-entity.service';
import { DependenciesService } from './services/dependencies.service';
import { DataSchemaElementsService } from './services/data-schema-elements.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
    ...CoreServices,
    ...StoreServices,
    UserIsAdminGuard,
    PendingChangesGuard,
    AggregateEntityService,
    DependenciesService,
    DataSchemaElementsService
  ],
  declarations: [
    UserInfoComponent,
    HeaderComponent,
    NotFoundComponent,
    NotAuthorizedComponent,
  ],
  exports: [
    UserInfoComponent,
    HeaderComponent,
  ]
})
export class CoreModule { }
