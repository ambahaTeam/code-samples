import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NodeSummaryEditComponent } from './node-summary-edit.component';

describe('UserEditFormComponent', () => {
  let component: NodeSummaryEditComponent;
  let fixture: ComponentFixture<NodeSummaryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeSummaryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeSummaryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
