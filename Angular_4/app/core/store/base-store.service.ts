import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { AppState, AppDomains } from '../../reducers';
import { createSelector } from 'reselect';

import * as fromDomain from '../../reducers/domain.reducer';
import * as appActions from '../../actions/app.actions';
import * as domainActions from '../../actions/domain.actions';
import { CloneNodeAction, DeleteNodeAction } from '../../actions/repository.actions';
import { ErrorAction } from '../../actions/error.actions';

import { hasPendingChanges } from '../../reducers/app.reducer';
import { canWrite } from '../../reducers/system-info.reducer';
import { toFolderEntityName  } from '../../prodapp-core/utils';
import { RepositoryNode } from '../../prodapp-core/repository';
import { DialogService } from '../../shared/dialog';

import { DomainObjectBase, EntityType, FolderEntityTypes } from '../../prodapp-core/model';
import { IEntityKeyInfoResult } from '../../prodapp-core/interfaces/IEntityKeyInfoResult';
import { RoutingTable } from '../../app.routing.table';
import {CLONE, CLONE_CONFIRMATION, DELETE, DELETE_CONFIRMATION} from '../../prodapp-core/constants';

/**
 * A base class for all injectable store services.
 *
 * @export
 * @abstract
 * @class StoreServiceBase
 * @template T - the type of state this service provides access to.
 */
export abstract class StoreServiceBase<T> {

  /**
   * When overridden by a service, gets the slice of application state this service encapsulates.
   *
   * @protected
   * @abstract
   *
   * @memberof StoreServiceBase
   */
  protected abstract readonly state = (state: AppState): T => { return null; };

  /**
   * Gets the application state.
   *
   * @protected
   *
   * @memberof StoreServiceBase
   */
  protected readonly appState = (state: AppState) => state;

  /**
   * Gets the selected entities slice of application state that all derived
   * services can share.
   *
   * @protected
   *
   * @memberof StoreServiceBase
   */
  protected readonly selectedState = (state: AppState) => state.selected;

  /**
   * Gets the global application slice of state that all derived
   * services can share.
   *
   * @protected
   *
   * @memberof StoreServiceBase
   */
  protected readonly applicationState = (state: AppState) => state.app;

  /**
   * Gets the global system slice of state that all derived
   * services can share.
   *
   * @protected
   *
   * @memberof StoreServiceBase
   */
  protected readonly systemState = (state: AppState) => state.system;

  /**
   * Gets a flag that indicates whether there are pending changes.
   *
   * @protected
   * @memberof StoreServiceBase
   */
  protected readonly pendingChanges = (state: AppState) => hasPendingChanges(state.app);

  private readonly _repositoryState = (state: AppState) => state.repository;

  /**
   * Gets a flag that indicates whether the current user is read-only or not.
   *
   * @protected
   * @memberof StoreServiceBase
   */
  protected readonly canEdit = (state: AppState) => canWrite(state.system);

  /**
   * Creates an instance of StoreServiceBase.
   * @param {Store<AppState>} store - the injected application store.
   * @param {DialogService} dialog - the injected dialog service.
   *
   * @memberof StoreServiceBase
   */
  protected constructor(
    public readonly store: Store<AppState>,
    public readonly dialog: DialogService) {
  }

  /**
   * Provides access to the slice of the domain state for the specified entity type.
   *
   * @protected
   * @template TEntity - the type of domain objects in this domain state.
   * @param {EntityType} type - the entity type for which to get the domain state slice.
   * @returns - the slice of the domain state for the specified entity type.
   *
   * @memberof StoreServiceBase
   */
  protected getDomainState<TEntity extends DomainObjectBase>(type: EntityType) {
    return createSelector<AppState, AppState, fromDomain.DomainState<TEntity>>(this.appState, (state) => state[AppDomains[type]]);
  }

  /**
   * Gets a domain entity from the domain state.
   *
   * @protected
   * @template TEntity - the type of domain entity to get from the domain state.
   * @param {number} id - the unique entity id (key).
   * @param {EntityType} type - the entity type that identifies the slice of the domain store to look.
   * @returns - the domain entity that matches the specified id and type.
   *
   * @memberof StoreServiceBase
   */
  protected getDomainObject<TEntity extends DomainObjectBase>(id: number, type: EntityType) {
    return createSelector(this.getDomainState<TEntity>(type), (state) => state.entities.getValue(id));
  }

  /**
   * Gets an observable of the currently selected repository node.
   *
   * @returns {Observable<RepositoryNode>} - the observable of the repository node that is
   * currently selected in the repository tree.
   * @memberof StoreServiceBase
   */
  private selectedRepositoryNode(): Observable<RepositoryNode> {
    return this.store.select(
      createSelector(this._repositoryState, (state) => state.entities.Selected));
  }

  /**
   * Clears pending changes flag in the application state.
   *
   * @memberof StoreServiceBase
   */
  public clearPendingChanges = () =>
    this.store.dispatch(new appActions.ClearPendingChangesAction());

  /**
   * Sets pending changes flag in the application state.
   *
   * @memberof StoreServiceBase
   */
  public setPendingChanges = () =>
    this.store.dispatch(new appActions.SetPendingChangesAction());

  /**
   * Displays delete confirmation dialog and dispatches store action to
   * delete an entity represented by the specified repository node if confirmed.
   *
   * @param {RepositoryNode} [node] - the optional repository node to delete. If not specified,
   * the currently selected node will be deleted.
   * @memberof StoreServiceBase
   */
  public delete(node?: RepositoryNode) {

    if (!node) {
      this.selectedRepositoryNode()
        .subscribe((current) => node = current)
        .unsubscribe();
    }

    if (node) {
      const entityTypeName = toFolderEntityName(node.type, node.isServiceVersion).toLowerCase();
      const entityName = node.isServiceVersion ?
        `Version ${(<any>node).VersionNumber}` :
        node.Name;
      const message = `${DELETE_CONFIRMATION} ${entityTypeName} "${entityName}" ?`;
      this.confirm(DELETE, message, () => {
        if (node.type === FolderEntityTypes.VirtualService) {
          node.isVirtual = true;
        }
        this.store.dispatch(new DeleteNodeAction(node));
      });
    }
  }

  /**
   * Displays clone confirmation dialog and dispatches store action to
   * clone an entity represented by the specified repository node if confirmed.
   *
   * @param {RepositoryNode} [node] - the optional repository node to clone. If not specified,
   * the currently selected node will be deleted.
   * @memberof StoreServiceBase
   */
  public clone(node?: RepositoryNode) {
    if (!node) {
      this.selectedRepositoryNode()
        .subscribe((current) => node = current)
        .unsubscribe();
    }
    if (node) {
      const entityTypeName = toFolderEntityName(node.type, node.isServiceVersion).toLowerCase();
      const entityName = node.isServiceVersion ?
        `Version ${(<any>node).VersionNumber}` :
        node.Name;
      const message = `${CLONE_CONFIRMATION} ${entityTypeName} "${entityName}" ?`;
      this.confirm(CLONE, message, () => {
        this.store.dispatch(new CloneNodeAction(node));
      });
    }
  }

  /**
   * Shows confirmation dialog, and if action confirmed - executes callback function
   *
   * @param {string} title - confirmation dialog title
   * @param {string} message - confirmation dialog message
   * @param {Function} [onConfirmCallback=undefined] - confirm action callback function
   * @param {Function} [onCancelCallback=undefined] - cancel action callback function
   * @memberof StoreServiceBase
   */
  public confirm(title: string, message: string, onConfirmCallback: Function = undefined, onCancelCallback: Function = undefined) {
    this.dialog.confirm(title, message).afterClosed().subscribe(confirm => {
      if (confirm && onConfirmCallback) {
        onConfirmCallback();
      } else if (onCancelCallback) {
        onCancelCallback();
      }
    })
  }

  /**
   * Displays an alert dialog and optionally executed a callback function
   * when dialog is closed.
   *
   * @param {string} title - the alert title.
   * @param {string} message - the alert message to display.
   * @param {() => void} [callback] - an optional callback function to execute when alert closes.
   * @memberof StoreServiceBase
   */
  public alert(title: string, message: string, callback?: () => void) {
    this.dialog.alert(title, message)
      .afterClosed()
      .subscribe(() => callback ? callback() : null)
      .unsubscribe();
  }

  /**
   * Dispatches error action
   *
   * @param {string} message - error message
   * @memberof StoreServiceBase
   */
  showError(message: string) {
    const error = new Error(message);
    error.name = null;
    this.store.dispatch(new ErrorAction({
      error
    }));
  }

  /**
   * Gets an observable that can be used to monitor pending changes within the application.
   */
  public hasPendingChanges(): Observable<boolean> {
    return this.store.select(createSelector(this.appState, this.pendingChanges));
  }

  /**
   * Gets an observable that can be used to monitor whether the current user has write permissions.
   */
  public canWrite(): Observable<boolean> {
    return this.store.select(createSelector(this.appState, this.canEdit));
  }

   /**
   * Dispatch go to action for routing to repository node
   * @param {number} id - entity id
   * @param {FolderEntityTypes} type - folder entity type
   * @memberof NodesStoreService
   */
  public goToRepositoryNode(id: number, type: FolderEntityTypes) {
    this.store.dispatch(RoutingTable.navigateToEntityAction(id, type));
  }

  /**
   * Dispatches go to action for routing to virtual service version repository node
   *
   * @param {number} id - the service version id
   * @param {number} serviceId - the parent service id
   *
   * @memberof StoreServiceBase
   */
  public goToVirtualServiceVersionRepositoryNode(id: number, serviceId: number) {
    this.store.dispatch(RoutingTable.navigateToServiceVersionAction(id, serviceId, true));
  }

  /**
   * Dispatch update of key info for entity
   *
   * @param {EntityType} domain - domain type
   * @param {IEntityKeyInfoResult<DomainObjectBase>} keyInfoResult - key info result with updated entity
   * @memberof StoreServiceBase
   */
  public updateKeyInfo(domain: EntityType, keyInfoResult: IEntityKeyInfoResult<DomainObjectBase>) {
    this.store.dispatch(new domainActions.UpdateEntityKeyInfoAction({
      domain,
      entity: keyInfoResult.Entity,
      keyInfo: keyInfoResult.KeyInfo
    }));
  }

  /**
   * Loads an entity into the app state. If entity is already loaded, this method
   * does nothing.
   *
   * @param {number} id - the id of entity to load.
   * @param {EntityType} type - the type of entity to load.
   * @memberof StoreServiceBase
   */
  public loadEntity(id: number, type: EntityType): void {
    this.store.select(this.getDomainObject(id, type))
      .take(1)
      .subscribe( (entity) => {
        if (!entity) {
          this.store.dispatch(new domainActions.LoadDomainEntityAction( { id, domain: type }));
        }
      });
  }

  /**
   * Gets the entity from the application state. This method returns an entity that already
   * exists in the state. To load an entity into the state, use loadEntity() method.
   *
   * @template TEntity - the type of entity to return.
   * @param {number} id - the id of entity to get from the app state.
   * @param {EntityType} type - the type of entity to get from the app state.
   * @returns {DomainObjectBase} - the entity from the state if exists, null otherwise.
   * @memberof StoreServiceBase
   */
  public getEntity<TEntity extends DomainObjectBase>(id: number, type: EntityType): TEntity {

    let entity: DomainObjectBase = null;

    this.store.select(this.getDomainObject(id, type))
      .take(1)
      .subscribe((e) => entity = e);

    return <TEntity> entity;
  }
}
