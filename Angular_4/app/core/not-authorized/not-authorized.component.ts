import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sw-not-authorized',
  templateUrl: './not-authorized.component.html',
  styleUrls: ['./not-authorized.component.scss']
})
export class NotAuthorizedComponent implements OnInit {

  public message = 'Not Authorized';

  constructor() { }

  ngOnInit() {
  }

}
