import * as moment from 'moment';

import { OperationalStatus } from '../enum';
import { DomainObjectBase } from '../DomainObjectBase';

/**
 * Represents a single instance of "prodapp" node. All instances of the same node are sharing the same identities
 * and base addresses, and hosting the same virtual services.
*/
export class NodeInstance extends DomainObjectBase {

  /** Gets or sets the IP address of the hosting computer used to call the repository service. */
  public ClientAddress: string = null;

  /** Gets or sets the node instance installation details (host name, OS version, etc). */
  public Information: string = null;

  /** Gets or sets the last heartbeat time of the instance. */
  public LastHeartbeat: moment.Moment = null;

  /** Gets or sets the last status reported by the instance. */
  public LastReportedStatus: OperationalStatus = OperationalStatus.Idle;

  /** Gets or sets the NetBIOS name of the hosting computer. */
  public MachineName: string = null;

  /** Gets or sets the unique instance name. */
  public Name: string = null;

  /** Gets or sets the node identifier this instance belongs to. */
  public NodeId: number = null;

  /** Gets or sets the "prodapp" version number installed on the hosting computer. */
  public RuntimeVersion: string = null;

  /** Gets current effective status of the node. */
  public Status: OperationalStatus = OperationalStatus.Idle;
}

