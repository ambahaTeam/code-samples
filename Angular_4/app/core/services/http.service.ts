import { Injectable } from '@angular/core';
import {
  Http,
  Request,
  RequestMethod,
  RequestOptions,
  RequestOptionsArgs,
  Response,
  Headers
} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { convertFromWCFDateTimeDeep, convertToWCFDateTimeDeep } from '../../prodapp-core/utils';
import * as errors from '../../actions/error.actions';
import * as processing from '../../actions/processing.actions';

/**
 * Common HTTP service for different HTTP method calls
 *
 * @export
 * @class HttpService
 */
@Injectable()
export class HttpService {

  /**
   * Creates an instance of HttpService.
   * @param {Http} _http - class for performing of HTTP requests
   * @param {Store<any>} _store - injected store
   * @memberof HttpService
   */
  constructor(
    private readonly _http: Http,
    private readonly _store: Store<any>
  ) { }

  /**
   * HTTP GET method
   *
   * @param {string} url - requested URL
   * @param {RequestOptionsArgs} [options] - request options
   * @returns
   * @memberof HttpService
   */
  public get(url: string, options?: RequestOptionsArgs) {
    return this.requestHelper({ url: url, method: RequestMethod.Get }, options);
  };

  /**
   * HTTP POST method
   *
   * @param {string} url - requested URL
   * @param {Object} body - request body
   * @param {RequestOptionsArgs} [options] - request options
   * @returns {Observable<Response>}
   * @memberof HttpService
   */
  public post(url: string, body: Object, options?: RequestOptionsArgs): Observable<Response> {
    return this.requestHelper({ url: url, body: body, method: RequestMethod.Post }, options);
  }

  /**
   * HTTP PUT method
   *
   * @param {string} url - requested URL
   * @param {Object} body - request body
   * @param {RequestOptionsArgs} [options] - request options
   * @returns {Observable<Response>}
   * @memberof HttpService
   */
  public put(url: string, body: Object, options?: RequestOptionsArgs): Observable<Response> {
    return this.requestHelper({ url: url, body: body, method: RequestMethod.Put }, options);
  }

  /**
   * HTTP DELETE method
   *
   * @param {string} url - requested URL
   * @param {RequestOptionsArgs} [options] - request options
   * @returns {Observable<Response>}
   * @memberof HttpService
   */
  public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.requestHelper({ url: url, method: RequestMethod.Delete }, options);
  }

  /**
   * HTTP PATCH method
   *
   * @param {string} url - requested URL
   * @param {Object} body - request body
   * @param {RequestOptionsArgs} [options] - request options
   * @returns {Observable<Response>}
   * @memberof HttpService
   */
  public patch(url: string, body: Object, options?: RequestOptionsArgs): Observable<Response> {
    return this.requestHelper({ url: url, body: body, method: RequestMethod.Patch }, options);
  }

  /**
   * HTTP HEAD method
   *
   * @param {string} url - requested URL
   * @param {RequestOptionsArgs} [options] - request options
   * @returns {Observable<Response>}
   * @memberof HttpService
   */
  public head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.requestHelper({ url: url, method: RequestMethod.Head }, options);
  }

  /**
   * Executes request and returns observable with response data
   *
   * @private
   * @param {Request} req - executed request
   * @param {RequestOptionsArgs} [options] - request options
   * @returns {Observable<Response>}
   * @memberof HttpService
   */
  private request(req: Request, options?: RequestOptionsArgs): Observable<Response> {

    // Apply global transformation to request.
    if (!req.headers.has('Accept')) {
      req.headers.append('Accept', 'application/json');
    }

    return this._http.request(req, options)
      .mergeMap(res => {

        /**
         * Assume that response is JSON
         */
        let isJson = true;

        /**
         * Attempt to parse body as JSON and catch exception if thrown.
         */
        try {
          res.json();
        } catch (e) {
          isJson = false;
        }

        if (isJson) {
          const raw = res.json();

          if (typeof raw === 'object') {
            // Here we're going to overload the res.json method.
            // We've already pulled the JSON with res.json();
            // So here we can apply global transformations to the JSON object.
            res.json = () => {
              return this.deserialize(raw);
            };
          }
        }

        return Observable.of(res);
      })
      .catch(this.handleError);
  }

  /**
   * Prepares request data for calling
   *
   * @private
   * @param {RequestOptionsArgs} requestArgs
   * @param {RequestOptionsArgs} additionalOptions
   * @returns {Observable<Response>}
   * @memberof HttpService
   */
  private requestHelper(requestArgs: RequestOptionsArgs, additionalOptions: RequestOptionsArgs): Observable<Response> {
    let options: RequestOptions = new RequestOptions(requestArgs);
    if (additionalOptions) {
      options = options.merge(additionalOptions);
    }

    if (options.body) {
      options.body = this.serialize(options.body);
      options.headers = new Headers({ 'Content-Type': 'application/json' });
    }

    return this.request(new Request(options));
  }

  /**
   * Redirects to login, with current URL as redirect url.
   *
   * @private
   * @returns
   * @memberof HttpService
   */
  private redirectToLogin(): void {
    const [href, hash] = window.location.href.split('#');
    const path = window.location.pathname;
    const trailingSlash = href.charAt(href.length - 1) === '/' ? '' : '/';
    const redirect = `${href}${trailingSlash}Login.aspx?expired&ReturnUrl=${encodeURIComponent(path + '#' + hash)}`;
    window.location.assign(redirect);
  }

  /**
   * Handle HTTP errors
   *
   * @private
   * @memberof HttpService
   */
  private handleError = (error: Response, caught: Observable<Response>): Observable<any> => {

    // If 401 - user session has likely expired.
    // Need to redirect to login page.
    if (error.status === 401) {
      this.redirectToLogin();
      return Observable.of();
    }

    // Convert error to friendly error message;
    let serverError;

    try {
      serverError = error.json();
    } catch (e) {
      // Error body isn't json, might be XML. Need to display.
      serverError = error.text();
    }

    // Create error with message and custom name
    const errorWithMessage = new Error(serverError);
    errorWithMessage.name = `${error.status} - ${error.statusText}`;

    this._store.dispatch(new errors.ErrorAction({ error: errorWithMessage }));
    this._store.dispatch(new processing.HideProcessingAction());
    return Observable.throw(errorWithMessage);
  }

  /**
   * Deserialize responses from the server
   *
   * @private
   * @param {Object} obj
   * @returns
   * @memberof HttpService
   */
  private deserialize(obj: Object) {
    return convertFromWCFDateTimeDeep(obj);
  }

  /**
   * Serialize requests to the server.
   *
   * @private
   * @param {*} body
   * @returns
   * @memberof HttpService
   */
  private serialize(body: any) {
    return convertToWCFDateTimeDeep(body);
  }

}
