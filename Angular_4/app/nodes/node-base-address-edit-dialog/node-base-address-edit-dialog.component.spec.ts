import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeBaseAddressEditDialogComponent } from './node-base-address-edit-dialog.component';

describe('NodeBaseAddressEditDialogComponent', () => {
  let component: NodeBaseAddressEditDialogComponent;
  let fixture: ComponentFixture<NodeBaseAddressEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeBaseAddressEditDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeBaseAddressEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
