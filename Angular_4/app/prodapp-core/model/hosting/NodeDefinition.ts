import { Node } from './Node';
import { NodeAddress } from './NodeAddress';
import { NodeInstance } from './NodeInstance';
import { IdentityDefinitionBase } from '../identity';
import { Issuer } from '../security';

/**
 * Represents a "prodapp" node. This class is a part of node definition structure used for creating or updating
 * nodes with single service call.
*/
export class NodeDefinition extends Node {

  /** Gets collection of the node's base addresses. */
  public BaseAddresses: NodeAddress[] = null;

  /** Gets collection of the node's identities. */
  public Identities: IdentityDefinitionBase[] = null;

  /** Gets collection of registered node instances. This collection and its items cannot be updated. */
  public Instances: NodeInstance[] = null;

  /**
   * Gets or sets a flag indicating that node's properties are populated.
   * False indicates that entity itself is not changed and child entities need to be checked instead.
   */
  public IsPopulated = false;

  /** Gets collection of security token issuers that node trusts.  */
  public TrustedIssuers: Issuer[] = null;
}

