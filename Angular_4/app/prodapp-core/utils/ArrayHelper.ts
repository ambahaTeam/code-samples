import { SortOrder } from '../model/enum';
import { DomainObjectBase } from '../model/DomainObjectBase';
import { IComparer, DefaultComparer } from './Comparers';

export class ArrayHelper {

  /**
   * Sorts elements of an array by the specified key and direction.
   *
   * @param {any[]} data - the data to sort.
   * @param {string} sortField - the sort field (property) name.
   * @param {SortOrder} sortOrder - the sorting direction. The default value is {SortOrder.Ascending}.
   * @param {IComparer} [comparer=null] - the optional custom comparer to use for sorting.
   * @returns {any[]} - the sorted array.
  */
  public static sort(data: any[],
    sortField: string,
    sortOrder: SortOrder = SortOrder.Ascending,
    comparer: IComparer = null) {

    if (!data || data.length <= 1) {
      return data;
    }

    if (!comparer) {
      comparer = DefaultComparer.instance;
    }

    const direction = sortOrder === SortOrder.Descending ? 1 : -1;

    return data.sort((a: any, b: any) => {
      const first = a[sortField], second = b[sortField];
      return comparer.compare(first, second) * direction;
    });
  };

  /**
   * Adds an item to the end of an array and returns a new array.
   *
   * @static
   * @param {any[]} data - the original array on which item will be added.
   * @param {*} item - the item to add.
   * @param {boolean=false} ifNotExists - true to only add if item does not exists, false to add regardless.
   * The default value is false.
   * @returns {any[]} - the new array with added item.
   *
   * @memberof ArrayHelper
  */
  public static add(data: any[], item: any, ifNotExists = false): any[] {

    if (!data || (ifNotExists && data.indexOf(item) >= 0)) {
      return [item];
    } else {
      return data.concat(item);
    }
  }

  /**
   * Inserts an item at the specified index and returns the new array. If the specified index
   * is less than 0 the item will be added as the first element of the array. If the index is
   * greater than the array length, the item will be added as the last element.
   *
   * @static
   * @param {any[]} data - the original array to which an item will be inserted.
   * @param {number} index - the index within the array at which an element will be inserted.
   * @param {*} item - the item to insert.
   * @returns {any[]} - the new, updated array.
   *
   * @memberof ArrayHelper
   */
  public static insert(data: any[], index: number, item: any): any[] {

    if (!data || data.length === 0) {
      return [item];
    } else if (index < 0) {
      index = 0;
    } else if (index >= data.length) {
      return ArrayHelper.add(data, item);
    }

    return [
      ...data.slice(0, index),
      item,
      ...data.slice(index)
    ];
  }

  /**
   * Replaces an array item with the new item and returns the new array.
   *
   * @static
   * @param {any[]} data - the original array in which to replace an item.
   * @param {*} item - the item that will be replaced.
   * @param {*} newItem - the item with which to replace.
   * @returns {any[]} - the new, updated array.
   *
   * @memberof ArrayHelper
   */
  public static replaceItem(data: any[], item: any, newItem: any): any[] {

    if (!data || data.length === 0) {
      return data;
    }

    return ArrayHelper.replaceAtIndex(data, data.indexOf(item), newItem);
  }

  /**
   * Replaces an array item at the specified index and returns the new array.
   *
   * @static
   * @param {any[]} data - the original array in which to replace an item.
   * @param {number} index - the index within the array at which an element will be replaced.
   * @param {*} item - the item with which to replace.
   * @returns {any[]} - the new, updated array.
   *
   * @memberof ArrayHelper
   */
  public static replaceAtIndex(data: any[], index: number, item: any): any[] {

    if (!data || data.length === 0 || index < 0 || index >= data.length) {
      return data;
    }

    return [
      ...data.slice(0, index),
      item,
      ...data.slice(index + 1)
    ];
  }

  /**
   * Removes an item from an array and returns the new array.
   *
   * @static
   * @param {any[]} data - the original array from which to remove an item.
   * @param {*} item - the item to remove.
   * @returns {any[]} - the new, updated array.
   *
   * @memberof ArrayHelper
   */
  public static removeItem(data: any[], item: any): any[] {

    if (!data || data.length === 0) {
      return data;
    }

    return ArrayHelper.removeAtIndex(data, data.indexOf(item));
  }

   /**
   * Removes an item from an array at the specified index and returns the new array.
   *
   * @static
   * @param {any[]} data - the original array from which to remove an item.
   * @param {number} index - the index at which to remove the element.
   * @returns {any[]} - the new, updated array.
   *
   * @memberof ArrayHelper
   */
  public static removeAtIndex(data: any[], index: number): any[] {

    if (!data || data.length === 0 || index < 0 || index >= data.length) {
      return data;
    }

    return [
      ...data.slice(0, index),
      ...data.slice(index + 1)
    ];
  }

  /**
   * Creates and returns the union of two arrays that doesn't have duplicates.
   * @param a - the first array.
   * @param b - the second array.
   */
  public static union(a: any[], b: any[]): any[] {
    if (!a) {
      return b;
    } else if (!b) {
      return a;
    } else {
      return a.concat(b.filter(function (item) {
        return a.indexOf(item) < 0;
      }));
    }
  }

  /**
   * Removes many elements from array by some condition
   *
   * @static
   * @param {Array<any>} source - entities array
   * @memberof FolderEntityTypesStoreService
   */
  /**
   * Removes many elements from array by some condition
   *
   * @static
   * @param {Array<any>} source - source array
   * @param {(element: any) => boolean} conditionChecked - condition callback. If true - element is being removed from array.
   * @memberof ArrayHelper
   */
  static removeMany(source: Array<any>, conditionChecked: (element: any) => boolean) {
    for (let i = 0; i < source.length; i++) {
      const node = source[i];
      if (conditionChecked(node)) {
        source.splice(i, 1);
        i--;
      }
    }
  }

  /**
   * Replaces existing item in collection or adds new one.
   * Item is being searched by Id property.
   *
   * @static
   * @template T - type which extends DomainObjectBase
   * @param {T[]} sourceElements - source array
   * @param {T} entity - updated entity
   * @returns {T[]} - updated array
   * @memberof ArrayHelper
   */
  static replaceOrAddItem<T extends DomainObjectBase>(sourceElements: T[], entity: T): T[] {

    if (!sourceElements) {
      return [entity];
    }

    const index = sourceElements.findIndex((el) => el.Id === entity.Id);

    let entities;

    if (index < 0) {
      entities = ArrayHelper.add(sourceElements, entity);
    } else {
      entities = ArrayHelper.replaceAtIndex(sourceElements, index, entity);
    }
    return entities;
  }
}
