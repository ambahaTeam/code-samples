import { NodeAddressKind } from '../enum';
import { DomainObjectBase } from '../DomainObjectBase';
import { AzureSecurityConfiguration } from '../security';

/** Represents a base address of "prodapp" node. */
export class NodeAddress extends DomainObjectBase {

  /** Gets or sets the node's base address. */
  public Address: string = null;

  /** Gets or sets the additional configuration required by certain address kinds.  */
  public Configuration: AzureSecurityConfiguration = null;

  /** Gets or sets the address kind like physical or cloud-based. Some kinds require an additional configuration. */
  public Kind: NodeAddressKind = NodeAddressKind.Physical;

  /** Gets or sets identifier of the node this address belongs to.  */
  public NodeId: number = null;
}

