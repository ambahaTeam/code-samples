import { DomainState } from './domain.reducer';
import { NodeDefinition, EndpointSummary, MonitoringActivity, MonitoringActivitiesRequest } from '../prodapp-core/model';
import { IDictionary, Dictionary } from '../prodapp-core/utils/Dictionary';
import * as nodes from '../actions/node.actions';
import * as repository from '../actions/repository.actions';

/**
 * Node State
 *
 * @export
 * @class State
 * @extends {DomainState<NodeDefinition>}
 * @implements {State}
 */
export class State extends DomainState<NodeDefinition> implements State {

  /**
   * Creates an instance of State.
   * @param {IDictionary<number, NodeDefinition>} entities - the dictionary for storing node entities
   * loaded into the state.
   * @param {EndpointSummary[]} [endpoints=new Array<EndpointSummary>()] - the currently selected node endpoints summary.
   * @param {MonitoringActivitiesRequest} [monitoringActivityRequest=new MonitoringActivitiesRequest()] - the monitoring
   * activities search parameters.
   * @param {MonitoringActivity[]} [activities=new Array<MonitoringActivity>()] - the monitoring activity for the currently selected node.
   * @memberof State
   */
  constructor(entities: IDictionary<number, NodeDefinition> = new Dictionary<number, NodeDefinition>(),
              public readonly endpoints: EndpointSummary[] = new Array<EndpointSummary>(),
              public readonly monitoringActivityRequest: MonitoringActivitiesRequest = new MonitoringActivitiesRequest(),
              public readonly activities: MonitoringActivity[] = new Array<MonitoringActivity>()) {
      super(entities);
  }
}

/**
 * Manages NodeDefinition state
 *
 * @export
 * @param {any} [state=new State()]
 * @param {nodes.Actions} action
 * @returns {State}
 */
export function reducer(state = new State(), action: nodes.Actions | repository.Actions): State {

  switch (action.type) {
    case repository.ActionTypes.GO_TO_NODE:
      return Object.assign({}, state,
        {
          monitoringActivityRequest: new MonitoringActivitiesRequest(),
          activities: new Array<MonitoringActivity>()
        });
    case nodes.ActionTypes.GET_NODE_ENDPOINTS_SUCCESS:
      return new State(state.entities, action.payload.endpoints, state.monitoringActivityRequest, state.activities);
    case nodes.ActionTypes.GET_NODE_ACTIVITY_LOGS_SUCCESS:
      return new State(state.entities, state.endpoints, action.payload.request, action.payload.activities);
    default:
      return state;
  }
}
