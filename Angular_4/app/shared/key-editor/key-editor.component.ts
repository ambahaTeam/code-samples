import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AppState } from '../../reducers';

import { FormValidators, ValidationMessages } from '../../shared/validation';
import { EditComponentBase } from '../../shared/components';
import { KeyInfo } from '../../prodapp-core/model/KeyInfo';
import { DialogService } from '../dialog/dialog.service';
import { KeyEditDialogComponent, KeyEditDialogData } from '../key-edit-dialog/key-edit-dialog.component';

/**
 * A component that handles the {KeyInfo} edit.
 *
 * @export
 * @class KeyEditorComponent
 * @extends {EditComponentBase<KeyInfo>}
 */
@Component({
  selector: 'sw-key-editor',
  templateUrl: './key-editor.component.html',
  styleUrls: ['./key-editor.component.scss'],
})
export class KeyEditorComponent extends EditComponentBase<KeyInfo> {

  /**
   * The form to bind to
   *
   * @type {FormGroup}
   * @memberof KeyEditorComponent
   */
  @Input()
  form: FormGroup;

  /**
   * Sets the modal dialog state.
   * True to display the dialog, false otherwise.
   *
   * @type {boolean}
   * @memberof IdentityEditComponent
   */
  @Input() open: boolean;

  /**
   * Sets the dialog header.
   *
   * @type {string}
   * @memberof IdentityEditComponent
   */
  @Input() header: string;

  /**
   * {DomainObjectBase} instance this form is bound to.
   *
   * @type {DomainObjectBase}
   * @memberof IdentityEditComponent
   */
  @Input() data: KeyInfo;

  /**
   * Fired when user clicks Save button.
   *
   * @memberof IdentityEditComponent
   */
  @Output() commit = new EventEmitter<KeyInfo>();

  /**
   * Fired when a requested action has been cancelled.
   *
   * @memberof IdentityEditComponent
   */
  @Output() cancel = new EventEmitter<KeyInfo>();

  /**
   * Gets or sets readonly setting for component
   *
   * @memberof KeyEditorComponent
   */
  @Input() readOnly = false;

  /**
   * Creates an instance of KeyEditorComponent.
   * @param {Store<AppState>} store - the app store to inject.
   * @param {FormBuilder} formBuilder - the form builder instance to use for building HTML form.
   * @param {DialogService} dialog - dialog service
   * @memberof KeyEditorComponent
   */
  constructor(
    store: Store<AppState>,
    formBuilder: FormBuilder,
    private readonly dialog: DialogService
  ) {
      super(store, formBuilder);
      this.populateValidationMessages();
  }

  /**
   * Builds an HTML form for editing {KeyInfo} data.
   *
   * @param {(isCreate: boolean) => void} callback
   * @returns
   * @memberof KeyEditorComponent
   */
  buildForm(callback: (isCreate: boolean) => void) {
    callback(this.data.Id <= 0);
    return this.formBuilder.group({
      NameLabel: [this.data.NameLabel],
      Name: [this.data.Name, FormValidators.forName()],
      Key: [this.data.Key, FormValidators.forGuid()]
    });
  }

  /**
   * Opens key edit dialog
   *
   * @memberof KeyEditorComponent
   */
  openDialog() {
    this.dialog.open<KeyEditDialogComponent, KeyEditDialogData>(KeyEditDialogComponent, {
        data: {
          entity: this.data,
          header: this.header,
          readOnly: this.readOnly
        }
    }).afterClosed().subscribe(this.onKeyEditDialogClose);
  }

  /**
   * Handler for dialog close event
   *
   * @memberof KeyEditorComponent
   */
  onKeyEditDialogClose = (key: KeyInfo) => {
    if (key) {
      const keyInfo = Object.assign(new KeyInfo(key.Key, this.form.value.Name), {Valid: this.form.valid});
      this.commit.emit(keyInfo);
    }
  }

  /**
   * Handler for name change event
   *
   * @memberof KeyEditorComponent
   */
  nameChanged() {
    const keyInfo = Object.assign({}, this.data, this.form.value, {Valid: this.form.valid});
    this.commit.emit(keyInfo);
  }

   /**
   * Populates error messages for HTML form validation.
   *
   * @private
   * @memberof KeyEditorComponent
   */
  private populateValidationMessages() {

    this.errors.Name = '';
    this.errors.Key = '';

    this.messages.Key = {
      required: ValidationMessages.keyRequired,
      pattern: ValidationMessages.keyInvalid
    };

    this.messages.Name = {
      required: ValidationMessages.nameRequired,
      pattern: ValidationMessages.nameInvalid
    };

  }
}
