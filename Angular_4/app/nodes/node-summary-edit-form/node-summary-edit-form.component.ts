import { Component, Input, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NodeDefinition } from '../../prodapp-core/model';
import { FormErrors } from '../../shared/validation';
import { KeyInfo } from '../../prodapp-core/model/KeyInfo';

@Component({
  selector: 'sw-node-summary-edit-form',
  templateUrl: './node-summary-edit-form.component.html',
  styleUrls: ['./node-summary-edit-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NodeSummaryEditFormComponent {

  protected _node: NodeDefinition;

  /**
   * The the form to bind to
   *
   * @type {FormGroup}
   * @memberof NodeSummaryEditComponent
   */
  @Input()
  form: FormGroup;

  /**
   * The errors object
   *
   * @type {*}
   * @memberof NodeSummaryEditComponent
   */
  @Input()
  errors: FormErrors<NodeDefinition>;

  /**
   * Key edit data object
   *
   * @type {KeyInfo}
   * @memberof NodeSummaryEditFormComponent
   */
  @Input()
  keyEditData: KeyInfo;

  /**
   * Event emitter for key change event
   *
   * @type {EventEmitter<KeyInfo>}
   * @memberof NodeSummaryEditFormComponent
   */
  @Output()
  keyInfoChanged = new EventEmitter<KeyInfo>();

  /**
   * Gets or sets readonly setting for component
   *
   *
   * @memberof NodeSummaryEditFormComponent
   */
  @Input()
  readonly = false;

  /**
   * Gets or sets the flag which means that node was just being created, but not saved
   *
   * @memberof NodeSummaryEditFormComponent
   */
  @Input()
  isNew = false;

  /**
   * Setter for data property. When data is changed - we want to patch the form value here & init key edit data.
   *
   * @memberof NodeSummaryEditComponent
   */
  @Input()
  set data(node: NodeDefinition) {
    if (node && this.form) {
      this.keyEditData = new KeyInfo(node.Key, node.Name, 'Name');
      this.form.patchValue(node);
      this._node = node;
    }
  }

  /**
   * Getter for data property.
   *
   * @readonly
   *
   * @memberof NodeSummaryEditFormComponent
   */
  get data(){
    return this._node;
  }

  /**
   * Emits event that key was changed
   *
   * @param {KeyInfo} key
   *
   * @memberof NodeSummaryEditFormComponent
   */
  commitKey(key: KeyInfo) {
    this.keyInfoChanged.emit(key);
  }

}
