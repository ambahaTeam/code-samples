import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Issuer, UserDefinition } from '../../prodapp-core/model';
import { DataTableRowEvent } from '../../shared/data-table/data-table-row-events';
import { DialogService } from '../../shared/dialog';
import { DELETE_CONFIRMATION, NEW_ID } from '../../prodapp-core/constants';
import {
  NodeTrustedIssuerEditWizardComponent,
  INodeTrustedIssuerEditWizardData
} from './wizard/node-trusted-issuer-edit-wizard/node-trusted-issuer-edit-wizard.component';

/**
 * Component with trusted named issuers for node entity
 *
 * @export
 * @class NodeTrustedNamedIssuersComponent
 */
@Component({
  selector: 'sw-node-trusted-named-issuers',
  templateUrl: './node-trusted-named-issuers.component.html',
  styles: ['']
})
export class NodeTrustedNamedIssuersComponent {

  private _newIssuerId = NEW_ID;

  /**
   * Gets or sets the array of issuers this component is bound to.
   *
   * @type {Issuer[]}
   * @memberof NodeTrustedNamedIssuersComponent
   */
  @Input()
  data: Issuer[];

  /**
   * Current user data
   *
   * @type {UserDefinition}
   * @memberof NodeTrustedNamedIssuersComponent
   */
  @Input()
  user: UserDefinition;

  /**
   * Gets or sets readonly setting for component
   *
   *
   * @memberof NodeSummaryEditFormComponent
   */
  @Input()
  readonly = false;

  /**
   * Update trusted issuer event emitter
   *
   *
   * @memberof NodeTrustedNamedIssuersComponent
   */
  @Output()
  update = new EventEmitter<Issuer>();

  /**
   * Delete trusted issuer event emitter
   *
   *
   * @memberof NodeTrustedNamedIssuersComponent
   */
  @Output()
  delete = new EventEmitter<Issuer>();

  /**
   * Creates an instance of NodeTrustedNamedIssuersComponent.
   * @param {DialogService} _dialogService - dialog service
   *
   * @memberof NodeTrustedNamedIssuersComponent
   */
  constructor(private readonly _dialogService: DialogService) {}

  /**
   * Create or update event handler
   *
   * @param {DataTableRowEvent<Issuer>} [event] - row event
   * @memberof NodeTrustedNamedIssuersComponent
   */
  onCreateOrUpdate(event?: DataTableRowEvent<Issuer>) {
    const issuer = event && event.data ? event.data : new Issuer();
    if (!event) {
      issuer.Id = this._newIssuerId--;
    }
    this._dialogService.open<NodeTrustedIssuerEditWizardComponent, INodeTrustedIssuerEditWizardData>(NodeTrustedIssuerEditWizardComponent, {
      data: {
        issuer,
        user: this.user
      }
    }).afterClosed().subscribe(updatedIssuer => {
      if (updatedIssuer) {
        this.update.emit(updatedIssuer);
      }
    });
  }

  /**
   * Delete event handler
   *
   * @param {DataTableRowEvent<Issuer>} event
   *
   * @memberof NodeTrustedNamedIssuersComponent
   */
  onDelete(event: DataTableRowEvent<Issuer>) {
    const header = 'Delete Trusted Node Issuer';
    const message = `${DELETE_CONFIRMATION} '${event.data.IssuerName}' identity?`;
    this._dialogService.confirm(header, message).afterClosed()
      .subscribe(confirm => {
        if (confirm) {
          this.delete.emit(event.data);
        }
    });
  }

}
