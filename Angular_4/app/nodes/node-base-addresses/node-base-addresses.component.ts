import { Component, Input, EventEmitter, Output } from '@angular/core';

import {
  NodeAddressSetUI,
  NodeAddressKind,
  IAzureIdentityData,
  NodeAddress} from '../../prodapp-core/model';
import { DELETE_CONFIRMATION, NEW_ID, ADD, MODIFY } from '../../prodapp-core/constants';
import {
  IdentityEditWizardComponent,
  IIdentityEditWizardData,
} from '../../shared/identities-grid/identity-edit-wizard/identity-edit-wizard.component';
import { DialogService } from '../../shared/dialog/dialog.service';
import { ImageFactory, StringHelper } from '../../prodapp-core/utils';
import { NodesStoreService } from '../../core/store';

/**
 * Component for node base addresses
 *
 * @export
 * @class NodeBaseAddressesComponent
 */
@Component({
  selector: 'sw-node-base-addresses',
  templateUrl: './node-base-addresses.component.html',
  styleUrls: ['./node-base-addresses.component.scss']
})
export class NodeBaseAddressesComponent {

  /**
   * Gets or sets the array of node addresses this component is bound to.
   *
   * @type {NodeAddressSetUI[]}
   * @memberof NodeBaseAddressesComponent
   */
  @Input()
  baseAddresses: NodeAddressSetUI[];

  /**
   * Current node id with default value (used at readonly view)
   *
   * @type {number}
   * @memberof NodeBaseAddressesComponent
   */
  @Input()
  nodeId = NEW_ID;

  /**
   * Update event emitter
   *
   * @type {EventEmitter<NodeAddress>}
   * @memberof NodeBaseAddressesComponent
   */
  @Output()
  update: EventEmitter<NodeAddress> = new EventEmitter<NodeAddress>();

  /**
   * Delete event emitter
   *
   * @type {EventEmitter<number>}
   * @memberof NodeBaseAddressesComponent
   */
  @Output()
  delete: EventEmitter<number> = new EventEmitter<number>();

  /**
   * Gets or sets readonly setting for component
   *
   *
   * @memberof NodeSummaryEditFormComponent
   */
  @Input()
  readonly = false;

  /**
   * Creates an instance of NodeBaseAddressesComponent.
   * @param {DialogService} _dialogService - dialog service
   * @param {NodesStoreService} _service - nodes store service
   * @memberof NodeBaseAddressesComponent
   */
  constructor(private readonly _dialogService: DialogService,
              private readonly _service: NodesStoreService) {}

  /**
   * Gets image address for provided node address kind value
   *
   * @param {number} status - status integer value
   * @returns - relative URL to appropriate status image
   *
   * @memberof NodeBaseAddressesComponent
   */
  getNodeAddressImage(status: number) {
    return ImageFactory.getNodeAddressImage(status);
  }

  /**
   * Grid update event handler
   *
   * @param {any} rowEvent - row event with updated node address
   *
   * @memberof NodeBaseAddressesComponent
   */
  onUpdate(rowEvent: { data: NodeAddress }) {
    switch (rowEvent.data.Kind) {
      case NodeAddressKind.Physical:
        this.update.emit(rowEvent.data);
      break;
      case NodeAddressKind.AzureServiceBus:
        this.openServiceBusAddressEditDialog(rowEvent.data);
      break;
    }
  }

  /**
   * Add event handler
   *
   * @memberof NodeBaseAddressesComponent
   */
  onAdd() {
    this.openServiceBusAddressEditDialog(new NodeAddress());
  }

  /**
   * Delete event handler (show dialog with confirmation before emitting of event)
   *
   * @param {{ data: NodeAddress }} rowEvent - row event data
   *
   * @memberof NodeBaseAddressesComponent
   */
  onDelete(rowEvent: { data: NodeAddress }) {
    const header = 'Delete Node Base Address';
    const message = `${DELETE_CONFIRMATION} base address ${rowEvent.data.Address}?`;
    this._dialogService.confirm(header, message).afterClosed()
      .subscribe(confirm => {
        if (confirm) {
          this.delete.emit(rowEvent.data.Id);
      }
    });
  }

  /**
   * Removes trailing slash from provided value
   *
   * @param {string} value - source value
   * @returns
   * @memberof NodeBaseAddressesComponent
   */
  removeTrailingSlash(value: string) {
    return StringHelper.trimEnd(value, '/');
  }

  /**
   * Opens edit dialog for node address with Azure type
   *
   * @private
   * @param {NodeAddress} address - node base address
   * @memberof NodeBaseAddressesComponent
   */
  private openServiceBusAddressEditDialog(address: NodeAddress) {

    const isModify = address.Id !== undefined;
    const azureIdentity =  this._service.convertBaseAddressToAzureIdentity(address, isModify);

    this._dialogService.open<IdentityEditWizardComponent, IIdentityEditWizardData>(IdentityEditWizardComponent, {
      data: {
        disabledTypes: [],
        identity: azureIdentity,
        address: address.Address,
        enableAddressEdit: true,
        enableAnonymousType: true,
        title: `${isModify ? MODIFY : ADD} Microsoft Azure Service Bus Address`,
        skipIdentitySelectTypeStep: true
      }
    }).afterClosed().subscribe((azureIdentityData: IAzureIdentityData) => {
      if (azureIdentityData) {
        const newAddress = this._service.convertAzureIdentityDataToNodeAddress(azureIdentityData, this.nodeId);
        this.update.emit(newAddress);
      }
    });
  }

}
