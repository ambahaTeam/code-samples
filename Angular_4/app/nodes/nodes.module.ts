import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { NodesRoutingModule } from './nodes.routing.module';
import { BindingsModule } from '../bindings/bindings.module';


import { NodeComponent } from './node/node.component';
import { NodePageComponent } from './containers/node-page/node-page.component';
import { NodeSummaryEditComponent } from './node-summary-edit/node-summary-edit.component';
import { NodeSummaryEditFormComponent } from './node-summary-edit-form/node-summary-edit-form.component';
import { NodeInstancesGridComponent } from './node-instances-grid/node-instances-grid.component';
import { CoreModule } from '../core/core.module';
import { NodeBaseAddressesComponent } from './node-base-addresses/node-base-addresses.component';
import { NodeTrustedNamedIssuersComponent } from './node-trusted-named-issuers/node-trusted-named-issuers.component';
import { NodeAdvancedComponent } from './node-advanced/node-advanced.component';
import { NodeBaseAddressEditDialogComponent } from './node-base-address-edit-dialog/node-base-address-edit-dialog.component';
import { NodeAuditPageComponent } from './containers/node-audit-page/node-audit-page.component';
import { NodeHostedServicesComponent } from './containers/node-hosted-services/node-hosted-services.component';
import { NodeHostedServicesViewComponent } from './node-hosted-services-view/node-hosted-services-view.component';
import { NodeEndpointsSummaryComponent } from './node-endpoints-summary/node-endpoints-summary.component';
import { NodePolicyViewDialogComponent } from './node-policy-view-dialog/node-policy-view-dialog.component';
import { NodeActivityLogsComponent } from './containers/node-activity-logs/node-activity-logs.component';
import { NodeActivityLogsSearchComponent } from './node-activity-logs-search/node-activity-logs-search.component';
import { NodeActivityLogsGridComponent } from './node-activity-logs-grid/node-activity-logs-grid.component';
import { NodeSummaryReadonlyComponent } from './node-summary-readonly/node-summary-readonly.component';
import { NodeDependenciesPageComponent } from './containers/node-dependencies-page/node-dependencies-page.component';
import { NodesComponent } from './nodes/nodes.component';
import { NodesSummaryComponent } from './nodes-summary/nodes-summary.component';
import { NodesSummaryPageComponent } from './containers/nodes-summary-page/nodes-summary-page.component';
import { NodeTrustedIssuerEditWizardComponent } from './node-trusted-named-issuers/wizard/node-trusted-issuer-edit-wizard/node-trusted-issuer-edit-wizard.component';

@NgModule({
  imports: [
    SharedModule,
    NodesRoutingModule,
    CoreModule,
    BindingsModule,
  ],
  declarations: [
    NodeComponent,
    NodePageComponent,
    NodeSummaryEditComponent,
    NodeSummaryReadonlyComponent,
    NodeSummaryEditFormComponent,
    NodeInstancesGridComponent,
    NodeBaseAddressesComponent,
    NodeTrustedNamedIssuersComponent,
    NodeAdvancedComponent,
    NodeBaseAddressEditDialogComponent,
    NodeAuditPageComponent,
    NodeHostedServicesComponent,
    NodeHostedServicesViewComponent,
    NodeEndpointsSummaryComponent,
    NodePolicyViewDialogComponent,
    NodeActivityLogsComponent,
    NodeActivityLogsSearchComponent,
    NodeActivityLogsGridComponent,
    NodeDependenciesPageComponent,
    NodesComponent,
    NodesSummaryComponent,
    NodesSummaryPageComponent,
    NodeTrustedIssuerEditWizardComponent,
  ],
  entryComponents: [
    NodeBaseAddressEditDialogComponent,
    NodePolicyViewDialogComponent,
    NodeTrustedIssuerEditWizardComponent
  ]
})
export class NodesModule {
}
