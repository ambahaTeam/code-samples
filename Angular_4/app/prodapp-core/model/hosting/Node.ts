import * as moment from 'moment';

import { NodeType } from '../enum/NodeType';
import { DomainObjectBase } from '../DomainObjectBase';
import { NodeSettings } from './NodeSettings';

/** Represent "prodapp" node instance. */
export class Node extends DomainObjectBase {

  /** Gets or sets identifier of folder that node belongs to. */
  public FolderId: number = null;

  /** Gets or sets node's heartbeat interval (time between two subsequent live checks). Default is two minutes.  */
  public HeartbeatInterval: moment.Duration = moment.duration(120000);

  /** Gets or sets flag indicating that node is enabled and hosting (ready to host) service endpoints. */
  public IsEnabled = false;

  /** Gets timestamp of the node's configuration. */
  public LastUpdate: moment.Moment = null;

  /** Gets or sets node friendly name. */
  public Name: string = null;

  /** Gets or sets this node's type. */
  public NodeType: NodeType = NodeType.IisMixedTransportsMode;

  /** Gets node's advanced configuration settings. */
  public Settings: NodeSettings = new NodeSettings();
}
