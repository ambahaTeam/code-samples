﻿playerApp.controller("playerController", function ($scope, playerService) {

    var vm = this;

    $scope.activeVideo = {};
    $scope.activeQuiz = null;
    $scope.activeQuizResult = null;

    $scope.dataLoaded = function() {
        return $scope.cLoaded;
    };

    $scope.init = function (course) {
        $scope.course = course;
        $scope.ccLoaded = true;

        var firstModuleOpened = false;
        for (var i = 0; i < $scope.course.Contents.length; i++) {
            if ($scope.course.Contents[i].Type === 'Module' && !firstModuleOpened) {
                $scope.course.Contents[i].Clicked = true;
                firstModuleOpened = true;
            }
            if ($scope.course.Contents[i].Video !== null) {
                $scope.course.Contents[i].Selected = true;
                $scope.activeVideo = $scope.course.Contents[i].Video;
                break;
            }
        }
    };

    $scope.checkModule = function(content) {
        return helper.checkModule($scope.course, content);
    };

    $scope.select = function(content) {
        for (var i = 0; i < $scope.course.Contents.length; i++) {
            $scope.course.Contents[i].Selected = $scope.course.Contents[i].Id === content.Id;
            if ($scope.course.Contents[i].Selected) {
                $scope.activeVideo = $scope.course.Contents[i].Video;
                updateVideoDock($scope.activeVideo);
            }
        }
    };

    $scope.updateVideoViewDate = function() {
        playerService.updateVideoViewDate($scope.activeVideo.Id)
            .then(function (data) {
                for (var i = 0; i < $scope.course.Contents.length; i++) {
                    if ($scope.course.Contents[i].Video !== null &&
                        $scope.course.Contents[i].Video.Id === data.VideoId) {
                        $scope.course.Contents[i].Video.DateViewed = data.DateViewed;
                        break;
                    }
                }
            });
    };

    $scope.activateQuiz = function(content) {
        $scope.activeQuiz = null;
        $scope.activeQuizResult = null;
        playerService.getQuiz(content.Quiz.Id)
            .then(function(data) {
                $scope.activeQuiz = data;
            });
    };

    $scope.submitActiveQuiz = function() {
        playerService.submitQuiz($scope.activeQuiz)
            .then(function(data) {
                $scope.activeQuizResult = data;
                for (var i = 0; i < $scope.course.Contents.length; i++) {
                    var quiz = $scope.course.Contents[i].Quiz;
                    if (quiz && quiz.Id === data.QuizId) {
                        quiz.DatePassed = data.DatePassed;
                        quiz.Passed = data.Passed;
                        break;
                    }
                }
            });
    };

    $scope.modalCloseText = function() {
        return $scope.activeQuizResult ? "Close" : "Cancel";
    };

    return vm;
});